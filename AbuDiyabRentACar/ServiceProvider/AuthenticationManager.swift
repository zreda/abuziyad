
//  Created by Zeinab Reda on 10/13/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import ObjectMapper

class AuthenticationManager: NSObject {
    
    func login(username:String,password:String ,completion:  @escaping (_ :LoginModelResponse?,_ :NSError?)->Void)
    {
        
        ServiceProvider.sendUrlString(showActivity: true, method: .post, URLString: .login, withQueryStringParameters: ["username":username as AnyObject,"password":password as AnyObject], withHeaders: [:]) { (response, error) in
            
            if error == nil
            {
                let responseJson = Helper.convertStringToDictionary(text: (response as! String).replace(target: "null", withString: ""))
                
                completion(LoginModelResponse(JSON:responseJson as! [String:Any]), error)
                
            }
            else
            {
                completion(nil, error)
                
            }
        }
    }
    
    
    func register(auth:RegisterRequestModel,completion:  @escaping (_ :RegisterModelResponse?,_ :NSError?)->Void)
    {
        
        let param:[String:AnyObject] = ["username":auth.username as AnyObject,"day_id_expiry_dt":auth.dayIdExpiryDt as AnyObject,"copy_no":auth.copyNo as  AnyObject,"first_name":auth.firstName as AnyObject,"second_name":auth.secondName as AnyObject ,"last_name":auth.lastName as AnyObject,"mobile_number":auth.mobileNumber as AnyObject,"phone_number":auth.phoneNumber as AnyObject,"license_no":auth.licenseNo as AnyObject,"ntnlty_code":auth.ntnltyCode as AnyObject,"day_lcns_end_date":auth.dayLcnsEndDate as AnyObject,"address_a":auth.addressA as AnyObject,"email":auth.email as AnyObject,"password":auth.password as AnyObject]
        
        ServiceProvider.sendUrl(showActivity: true, method: .post, URLString: .register, withQueryStringParameters: param, withHeaders: [:]) { (response, error) in
            
            if error == nil
            {
                completion(RegisterModelResponse(JSON:response as! [String:Any]), error)
                
            }
            else
            {
                completion(nil, error)
                
            }
            
        }
    }
    
    func getAllNationality(completion:  @escaping (_ :[NationalityModelResponse]?,_ :NSError?)->Void)
    {
        ServiceProvider.sendUrl(showActivity: true, method: .get, URLString: .nationality, withQueryStringParameters: nil, withHeaders: [:]) { (response, error) in
            
            if error == nil
            {
                completion(Mapper<NationalityModelResponse>().mapArray(JSONArray: response as! [[String : Any]]), error)
            }
            else
            {
                completion(nil, error)
                
            }
        }
    }
    
    
    func forgetPassword(email:String,completion:  @escaping (_ :ForgetPasswordModelResponse?,_ :NSError?)->Void)
    {
        ServiceProvider.sendUrl(showActivity: true, method: .post, URLString: .forgetPassword, withQueryStringParameters: ["your_email_address":email as AnyObject], withHeaders: [:]) { (response, error) in
            
            if error == nil
            {
                
                
                completion(ForgetPasswordModelResponse(JSON:response as! [String:Any]), error)
                
                
                
            }
            else
            {
                completion(nil, error)
                
            }
        }
    }
    
    
    func resetPassword(auth:ResetRequestModel,completion:  @escaping (_ :ResetPasswordModelResponse?,_ :NSError?)->Void)
    {
        
        let params = ["user_id":auth.userId as AnyObject,"username":auth.username as AnyObject,"password":auth.password as AnyObject,"con_password":auth.conPassword as AnyObject]
        ServiceProvider.sendUrl(showActivity: true, method: .post, URLString: .resetPassword, withQueryStringParameters: params, withHeaders: [:]) { (response, error) in
            
            if error == nil
            {
                
                
                completion(ResetPasswordModelResponse(JSON:response as! [String:Any]), error)
                
                
                
            }
            else
            {
                completion(nil, error)
                
            }
        }
    }
    
    
    func viewProfile(profileData:ProfileRequestModel,completion:  @escaping (_ :ProfileModelResponse?,_ :NSError?)->Void)
    {
        
        let param = ["user_machine_id":profileData.userMachineId as AnyObject, "id_no":profileData.idNo as AnyObject,  "first_name":profileData.firstName as AnyObject ,  "second_name":profileData.secondName as AnyObject , "last_name":profileData.lastName as AnyObject , "user_email":profileData.userEmail as AnyObject , "ntly":profileData.ntly as AnyObject , "mobile_no":profileData.mobileNo as AnyObject , "phone_no":profileData.phoneNo as AnyObject , "license_no":profileData.licenseNo as AnyObject , "address":profileData.address as AnyObject]
        
        ServiceProvider.sendUrl(showActivity: true, method: .post, URLString: .viewProfile, withQueryStringParameters: param, withHeaders: [:]) { (response, error) in
            
            if error == nil
            {
                completion(ProfileModelResponse(JSON:response as! [String:Any]), error)  
            }
            else
            {
                completion(nil, error)
                
            }
        }
    }
    
    
    func updateProfile(profileData:ProfileRequestModel,completion:  @escaping (_ :ProfileModelResponse?,_ :NSError?)->Void)
    {
        
        let param = ["user_machine_id":profileData.userMachineId as AnyObject, "id_no":profileData.idNo as AnyObject,  "first_name":profileData.firstName as AnyObject ,  "second_name":profileData.secondName as AnyObject , "last_name":profileData.lastName as AnyObject , "user_email":profileData.userEmail as AnyObject , "ntly":profileData.ntly as AnyObject , "mobile_no":profileData.mobileNo as AnyObject , "phone_no":profileData.phoneNo as AnyObject , "license_no":profileData.licenseNo as AnyObject , "address":profileData.address as AnyObject]
        
        ServiceProvider.sendUrl(showActivity: true, method: .post, URLString: .viewProfile, withQueryStringParameters: param, withHeaders: [:]) { (response, error) in
            
            if error == nil
            {
                completion(ProfileModelResponse(JSON:response as! [String:Any]), error)
                
                
                
            }
            else
            {
                completion(nil, error)
                
            }
        }
    }
    
}

