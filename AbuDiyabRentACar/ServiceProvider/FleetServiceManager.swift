//
//  FleetServiceManager.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 12/5/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation

import ObjectMapper

class FleetServiceManager: NSObject {
    
    func getFleets(cat_id:String ,completion:  @escaping (_ :[FleetModelRespopnse]?,_ :NSError?)->Void)
    {
        
        ServiceProvider.sendUrl(showActivity: true, method: .get, URLString: .fleet(catId: cat_id), withQueryStringParameters: ["cat_id":cat_id as AnyObject], withHeaders: [:]) { (response, error) in
            
            if error == nil
            {
                completion(Mapper<FleetModelRespopnse>().mapArray(JSONArray: response as! [[String : Any]]), error)
                
            }
            else
            {
                completion(nil, error)
                
            }
        }
    }
    
    
    func getFilters(completion:  @escaping (_ :[FilterModelResponse]?,_ :NSError?)->Void)
    {
        
        ServiceProvider.sendUrl(showActivity: true, method: .get, URLString: .filter, withQueryStringParameters: nil, withHeaders: [:]) { (response, error) in
            
            if error == nil
            {
            
                completion(Mapper<FilterModelResponse>().mapArray(JSONArray: response as! [[String : Any]]), error)

                
            }
            else
            {
                completion(nil, error)
                
            }
            
        }
    }
    
    func getAllNationality(completion:  @escaping (_ :[NationalityModelResponse]?,_ :NSError?)->Void)
    {
        ServiceProvider.sendUrl(showActivity: true, method: .post, URLString: .login, withQueryStringParameters: nil, withHeaders: [:]) { (response, error) in
            
            if error == nil
            {
                completion(Mapper<NationalityModelResponse>().mapArray(JSONArray: response as! [[String : Any]]), error)
                
                
            }
            else
            {
                completion(nil, error)
                
            }
        }
    }
    
    
    func forgetPassword(email:String,completion:  @escaping (_ :ForgetPasswordModelResponse?,_ :NSError?)->Void)
    {
        ServiceProvider.sendUrl(showActivity: true, method: .post, URLString: .forgetPassword, withQueryStringParameters: ["your_email_address":email as AnyObject], withHeaders: [:]) { (response, error) in
            
            if error == nil
            {
                
                
                completion(ForgetPasswordModelResponse(JSON:response as! [String:Any]), error)
                
                
                
            }
            else
            {
                completion(nil, error)
                
            }
        }
}
}
