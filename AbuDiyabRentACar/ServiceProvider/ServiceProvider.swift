//
//  ServiceProvider.swift
//  Intercom
//
//  Created by Zeinab Reda on 3/11/17.
//  Copyright © 2017 Zeinab Reda. All rights reserved.
//

import Alamofire
import SVProgressHUD

var urlEnvironment: AZUrls.UrlEnvironment {
    return .baseUrl
}


class ServiceProvider {
    

    static func sendUrl(showActivity:Bool,method:HTTPMethod,URLString: AZUrls.Path, withQueryStringParameters parameters: [String : AnyObject]?, withHeaders headers: [String : String]?, completionHandler completion:@escaping (_ :NSObject?,_ :NSError?) -> Void)
    {
        
        SVProgressHUD.show()

        var pathUrl = "\(urlEnvironment.getBaseUrl())\(URLString.absolutePath)"
            pathUrl += "&lang=\(LanguageManger.shared.currentLang)"
        

      
        Alamofire.request(pathUrl, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            debugPrint("Request: \(String(describing: response.request))")
            debugPrint("Headers: \(String(describing: response.request?.allHTTPHeaderFields))")
            
            if method == .post
            {
                debugPrint("Body Paramters: \(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue) ?? "")")
            }
            
            debugPrint("Response: \(String(describing: response.result.value))")
            
            debugPrint("Error: \(String(describing: response.error))")
            
            switch(response.result) {
            case .success(_):
                
                if response.response?.statusCode == 200
                {
                    if let data = response.result.value
                    {
                        completion(data as? NSObject ,nil)
                        
                    }
                }
    
                else
                {
                    completion(NSObject() ,NSError(domain: "No Internet Connection", code: (response.response?.statusCode)! , userInfo: [:]))
                    
                    Helper.showFloatAlert(title: "No Internet Connection".localized(), subTitle: "", type: Constants.AlertType.AlertError)
                    
                }
                
                SVProgressHUD.dismiss()
                
                break
                
            case .failure(_):
                
    
                    completion(NSObject() ,NSError(domain: "No Internet Connection", code: (response.response?.statusCode) ?? 500 , userInfo: [:]))
                    
                    Helper.showFloatAlert(title: "No Internet Connection", subTitle: "", type: Constants.AlertType.AlertError)
                    
                    SVProgressHUD.dismiss()

                
                break
                
            }
            
        }
        
    }
    
    
    
    static func sendUrlString(showActivity:Bool,method:HTTPMethod,URLString: AZUrls.Path, withQueryStringParameters parameters: [String : AnyObject]?, withHeaders headers: [String : String]?, completionHandler completion:@escaping (_ :NSObject,_ :NSError?) -> Void)
    {
        if showActivity
        {
            SVProgressHUD.show()
        }
        
        let pathUrl = "\(urlEnvironment.getBaseUrl())\(URLString.absolutePath)"
       
        
        Alamofire.request(pathUrl, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseString { (response) in
            debugPrint("Request: \(String(describing: response.request))")
            debugPrint("Headers: \(String(describing: response.request?.allHTTPHeaderFields))")
            debugPrint("Body Paramters: \(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue) ?? "")")
            
            debugPrint("Response: \(String(describing: response.result.value))")
            
            debugPrint("Error: \(String(describing: response.error))")
            
            switch(response.result) {
            case .success(_):
                if response.response?.statusCode == Constants.StatusCode.StatusOK
                {
                    
                    if let data = response.result.value{
                        completion(data as NSObject ,nil)
                        
                    }
                }
               
                else
                {
                    Helper.showFloatAlert(title: "Server Error , please try again", subTitle: "", type: Constants.AlertType.AlertError)
                    
                }
                SVProgressHUD.dismiss()
                
                break
                
            case .failure(_):
                
                Helper.showFloatAlert(title: "No Internet Connection", subTitle: "", type: Constants.AlertType.AlertError)
                
                SVProgressHUD.dismiss()
                
                break
                
            }
            
        }
        
    }
    
  static  func sendMultiPartUrl(showActivity:Bool,method:HTTPMethod,URLString: AZUrls.Path, withQueryStringParameters parameters: [String : AnyObject]?, withHeaders headers: [String : String]?,photos:[String:UIImage], completionHandler completion:@escaping (_ :NSObject,  _ :NSError?) -> Void)
    {
        if showActivity
        {
            SVProgressHUD.show()
        }
        let pathUrl = "\(urlEnvironment.getBaseUrl())\(URLString.absolutePath)"
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
          
            
            for (key, value) in photos {
                
                multipartFormData.append(UIImageJPEGRepresentation(value, 0.5)!, withName: key, fileName: "img.jpeg", mimeType: "image/jpg")
                
            }
            
            for (key, value) in parameters! {
                
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
                
                
            }
            
        }, to:pathUrl)
        {
            (result) in
            switch result {
            case .success(let upload, _, _):
                
                
                upload.uploadProgress(closure: { (Progress) in
                    debugPrint("Upload Progress: \(Progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    //self.delegate?.showSuccessAlert()
                    debugPrint("Request: \(String(describing: response.request))")
                    debugPrint("Headers: \(String(describing: response.request?.allHTTPHeaderFields))")
                    //                    print("Body Paramters: \(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue) ?? "")")
                    
                    debugPrint("Response: \(String(describing: response.result.value))")
                    
                    debugPrint("Error: \(String(describing: response.error))")
                    
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                        
                        SVProgressHUD.dismiss()
                        completion(response.result.value as! NSObject ,nil)
                        
                        
                    }
                }
                
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                debugPrint(encodingError)
                Helper.showAlert(type: Constants.AlertType.AlertError, title: "error", subTitle: "", closeTitle: "Cancel".localized())
                
            }
            
        }
        
    }
    
    
}
