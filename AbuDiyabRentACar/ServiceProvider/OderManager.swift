//
//  OderManager.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 12/10/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import ObjectMapper

class OderManager {
    
    
    func getOrderHistory(completion:  @escaping (_ :[OrderModelResponse]?,_ :NSError?)->Void)
    {
        let userId = String(((Helper.getObjectDefault(key: Constants.userDefault.userData) as! UserDataModel).iD as! Int))
        
        ServiceProvider.sendUrl(showActivity: true, method: .get, URLString: .orderHistory, withQueryStringParameters: ["user_machine_id":userId as AnyObject], withHeaders: [:]) { (response, error) in
            
            if error == nil
            {
                completion(Mapper<OrderModelResponse>().mapArray(JSONArray: response as! [[String : Any]]), error)
                
            }
            else
            {
                completion(nil, error)
                
            }
        }
    }
    
    
    
    func OrderByCredit(orderRequest:OrderByCreditModelRequest , completion:  @escaping (_ :PuchaseModelResponse?,_ :NSError?)->Void)
    {
        
        let param = ["user_id":orderRequest.userId as! AnyObject,"branch":orderRequest.branch as! AnyObject,"city":orderRequest.city as! AnyObject,"received_date":orderRequest.receivedDate as! AnyObject,"received_time":orderRequest.receivedTime as! AnyObject,"car_id":orderRequest.carId as! AnyObject,"number_of_days":orderRequest.numberOfDays as! AnyObject,"card_number":orderRequest.cardNumber as! AnyObject,"expirationYear":orderRequest.expirationYear as! AnyObject,"card_exp_month":orderRequest.cardExpMonth as! AnyObject,"card_csc":orderRequest.cardCsc as! AnyObject]
        
        
        ServiceProvider.sendUrl(showActivity: true, method: .post, URLString: .orderCredit, withQueryStringParameters: param, withHeaders: [:]) { (response, error) in
            
            if error == nil
            {
                
                completion(PuchaseModelResponse(JSON:response as! [String:Any]), error)
                
            }
            else
            {
                completion(nil, error)
                
            }
        }
    }
    
    
    
    func OrderCash(orderRequest:OrderByCreditModelRequest , completion:  @escaping (_ :PuchaseModelResponse?,_ :NSError?)->Void)
    {
        
        let param = ["user_id":orderRequest.userId as! AnyObject,"branch":orderRequest.branch as! AnyObject,"city":orderRequest.city as! AnyObject,"received_date":orderRequest.receivedDate as! AnyObject,"received_time":orderRequest.receivedTime as! AnyObject,"car_id":orderRequest.carId as! AnyObject,"number_of_days":orderRequest.numberOfDays as! AnyObject]
        
        
        ServiceProvider.sendUrl(showActivity: true, method: .post, URLString: .orderCash, withQueryStringParameters: param, withHeaders: [:]) { (response, error) in
            
            if error == nil
            {
                
                completion(PuchaseModelResponse(JSON:response as! [String:Any]), error)
                
            }
            else
            {
                completion(nil, error)
                
            }
        }
    }
    
    
    
    func OrderMemberShip(user_id:String,membership_name:String,fullname:String,city:String,address:String,passportPhoto:UIImage,userIdPhoto:UIImage,userLicensePhoto:UIImage,userVisaCardPhoto:UIImage , completion:  @escaping (_ :UpdateProfileModelResponse?,_ :NSError?)->Void)
    {
        
        
        let userId = String(describing: ((Helper.getObjectDefault(key: Constants.userDefault.userData) as! UserDataModel).iD as! Int))
        let param = ["user_id":userId as AnyObject,"membership_name":membership_name as AnyObject,"city":city as AnyObject,"fullname":fullname as AnyObject,"address":address as AnyObject]
        
        ServiceProvider.sendMultiPartUrl(showActivity: true, method: .post, URLString: .applyMembership, withQueryStringParameters: param, withHeaders: nil, photos: ["userPhoto":passportPhoto,"userIdPhoto":userIdPhoto ,"userLicensePhoto":userLicensePhoto ,"userVisaCardPhoto":userVisaCardPhoto]) { (response, error) in
            
            if error == nil
            {
                
                completion(UpdateProfileModelResponse(JSON:response as! [String:Any]), error)
                
            }
            else
            {
                completion(nil, error)
                
            }
        }
        
    }
}
