//
//  MembershipCell.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 12/8/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class MembershipCell: UITableViewCell {

  
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    internal var login: ((String) -> ())?
    
    var dataModel: AZMembershipModel?
    
    func setupCell(_ data: AZMembershipModel) {
        
        dataModel = data
        titleLbl.text = data.title
        headerView.backgroundColor =  Helper.hexStringToUIColor(hex: data.clrCode!)
        imgView.sd_setImage(with: URL(string: data.image!)!, placeholderImage: #imageLiteral(resourceName: "default_img"))
        
        var stringData: String = ""
        for item in data.tickers {
            stringData.append("✔︎ \(item.points!)\n")
        }
        
        contentLabel.text = stringData
    }
    
    @IBAction func orderMembership(_ sender: UIButton) {
        
        login?(dataModel?.title ?? "")
    }
}
