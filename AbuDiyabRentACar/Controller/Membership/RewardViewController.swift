//
//  RewardViewController.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 11/11/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class RewardViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var membershipTableView: UITableView!
    
    var membershipTypes: [AZMembershipModel]?
    
    
    // MARK: - Methods
    
    // MARK: - Init
    
    /**
     Initialize an instance of Reward Points View Controller
     - returns: RewardViewController Object
     */
    public static func create() -> RewardViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! RewardViewController
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "title_text".localized()
        descriptionTextView.text = "reward_points_text".localized()
        getData()
    }
    
    private func getData() {
        
        let membershipManager = AZMembershipRequest()
        membershipManager.getRewardsData()
        membershipManager.didGetMembershipRequest = { (data, error) in
            
            if let error = error {
                debugPrint(error.localizedDescription)
                Helper.showFloatAlert(title: error.localizedDescription, subTitle: "", type: Constants.AlertType.AlertError)
            } else {
                self.membershipTypes = data
                self.membershipTableView.reloadData()
            }
        }
    }
}


extension RewardViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return membershipTypes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MembershipCell") as! MembershipCell
        cell.setupCell(membershipTypes![indexPath.row])
        cell.login = { _ in
            self.navigationController?.pushViewController(SiginViewController.create(), animated: true)
        }
        return cell
    }
}
