//
//  MembershipViewController.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 12/8/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class MembershipViewController: UIViewController {

    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var membershipTableView: UITableView!
    let membershipManager = AZMembershipRequest()
    
    var membershipTypes: [AZMembershipModel]?
    
    
    public static func create() -> MembershipViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! MembershipViewController
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "title_text".localized()
        descriptionTextView.text = "membership_text".localized()
        self.hideKeyboard()
        getData()
    }
    
    private func getData() {
        
        membershipManager.getMembershipData()
        membershipManager.didGetMembershipRequest = { (data, error) in
            
            if let error = error {
                debugPrint(error.localizedDescription)
                Helper.showFloatAlert(title: error.localizedDescription, subTitle: "", type: Constants.AlertType.AlertError)
            } else {
                self.membershipTypes = data
                self.membershipTableView.reloadData()
            }
        }
    }
}

extension MembershipViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return membershipTypes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MembershipCell") as! MembershipCell
        cell.setupCell(membershipTypes![indexPath.row])
        cell.login = { _ in
            
            if !Helper.isKeyPresentInUserDefaults(key: Constants.userDefault.userData) {
                self.navigationController?.pushViewController(SiginViewController.create(), animated: true)
            } else {
                
                self.membershipManager.checkOrder()
                self.membershipManager.didCheckOrder = { (msg, error) in
                    if let error = error as NSError? {
                        if error.code == 123 {
                           
                            let vc = OrderMembershipViewController.create()
                            vc.membershipTypeObj = self.membershipTypes!
                            
                            self.navigationController?.pushViewController(vc, animated: true)
                            return
                        }
                        Helper.showFloatAlert(title: error.localizedDescription, subTitle:"" , type: Constants.AlertType.AlertError)
                    } else {
                        Helper.showFloatAlert(title: msg!, subTitle: "", type: Constants.AlertType.AlertError)
                    }
                }
            }
        }
        return cell
    }
}
