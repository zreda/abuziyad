//
//  OrderMembershipViewController.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 12/19/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import IQDropDownTextField

enum photoType
{
    case passport
    case visa
    case license
    case id
}
class OrderMembershipViewController: UITableViewController  {
    let picker = UIImagePickerController()

    @IBOutlet weak var idNumber: DesignableUITextField!
    @IBOutlet weak var name: DesignableUITextField!
    @IBOutlet weak var membershipType: IQDropDownTextField!
    
    @IBOutlet weak var address: DesignableUITextField!
    @IBOutlet weak var cities: IQDropDownTextField!
    @IBOutlet weak var licenseNumber: DesignableUITextField!
    
    @IBOutlet weak var mobileNumber: DesignableUITextField!
    var membershipTypeObj :[AZMembershipModel] = []
    
    var passportPhoto:UIImage?
    var idNumberPhoto:UIImage?
    var licensePhoto:UIImage?
    var visaPhoto:UIImage?
    var photo:photoType?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        title = "Order Membership".localized()
        mobileNumber.delegate = self
        licenseNumber.delegate = self
        
        var membershipList:[String] = []
        for (_,item) in membershipTypeObj.enumerated()
        {
            membershipList.append(item.title)
        }
        membershipType.itemList = membershipList
        picker.delegate = self
        self.idNumber.text = ((Helper.getObjectDefault(key: Constants.userDefault.userData) as! UserDataModel).userLogin )
        self.name.text = ((Helper.getObjectDefault(key: Constants.userDefault.userData) as! UserDataModel).displayName)

        let branchesRequest = AZBranchesRequest()
        branchesRequest.getBranchesList()
        branchesRequest.didGetBranchesList = { [weak self] (branches, error) -> Void in
            if let error = error {
                Helper.showFloatAlert(title: error.localizedDescription, subTitle: "", type: Constants.AlertType.AlertError)
            } else {
                if let branches = branches {
                    var citiesList: [String] = []
                    
                    for b in branches {
                        citiesList.append(b.city)
                    
                    }
                    self?.cities.itemList = citiesList
                   
                    
                }
            }
        }

    }

    public static func create() -> OrderMembershipViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! OrderMembershipViewController
    }
    
  
    @IBAction func addPassportPhotoBtnTapped(_ sender: Any) {
        photo = .passport
        openGalary()
    }
    
    @IBAction func addImageIDBtnTapped(_ sender: Any) {
        photo = .id

        openGalary()
    }
    
    @IBAction func addLicensePhotoBtnTapped(_ sender: Any) {
        photo = .license

        openGalary()
    }
    
    @IBAction func addVisaPhotoBtnTapped(_ sender: Any) {
        photo = .visa

        openGalary()
    }
    
    
    @IBAction func sendBtnTapped(_ sender: Any)
    {
        if name.text == "" || membershipType.selectedRow == -1 || cities.selectedRow == -1
        || passportPhoto == nil || licensePhoto == nil || visaPhoto == nil || idNumberPhoto == nil || address.text == ""
        {
            Helper.showFloatAlert(title: "All fields are required", subTitle: "", type: Constants.AlertType.AlertError)
        }
        else if (mobileNumber.text?.count)! < 10
        {
            Helper.showFloatAlert(title: "mobile number must be 10 numbers".localized(), subTitle: "", type: Constants.AlertType.AlertError)
            
        }
        else
        {
            OderManager().OrderMemberShip(user_id: idNumber.text!, membership_name: (membershipType.selectedItem ?? "")!, fullname: name.text!, city: (cities.selectedItem ?? "")!, address: address.text!, passportPhoto: passportPhoto!, userIdPhoto: idNumberPhoto!, userLicensePhoto: licensePhoto!, userVisaCardPhoto: visaPhoto!, completion: { (response, error) in
                if response != nil
                {
                    if response?.error == "true"
                    {
                        Helper.showFloatAlert(title: (response?.msg!)!, subTitle: "", type: Constants.AlertType.AlertError)
                    }
                    else
                    {
                        Helper.showFloatAlert(title: "membership success".localized(), subTitle: "", type: Constants.AlertType.AlertSuccess)
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }
                else
                {
                    Helper.showAlert(type: Constants.AlertType.AlertSuccess, title: "Something went wrong!".localized(), subTitle: "", closeTitle: "ok".localized())

                }
            })
        }
        
    }
    
    
     func openGalary() {
        
        let alert = UIAlertController(title: "Upload Image".localized(), message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Camera".localized(), style: UIAlertActionStyle.default, handler: { action in
            
            self.picker.sourceType = .camera
            self.present(self.picker, animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "Photos".localized(), style: UIAlertActionStyle.default, handler: { action in
            self.picker.sourceType = .photoLibrary
            self.present(self.picker, animated: true, completion: nil)
            
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
}

extension OrderMembershipViewController :  UIImagePickerControllerDelegate , UINavigationControllerDelegate , UITextFieldDelegate
{
    
    
    // Picker Image delegate functions
    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if (info[UIImagePickerControllerOriginalImage] as? UIImage) != nil {
            //  imageView.contentMode = .scaleAspectFit
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        NSLog("\(info)")
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            guard let resizedImg = image.resizeImage(newHeight: 100.0) else {
                dismiss(animated: true, completion: nil)
                return
            }
            if photo == photoType.id
            {
                idNumberPhoto = resizedImg

            }
            else if photo == photoType.visa
            {
                visaPhoto = resizedImg
            }
            else if photo == photoType.passport
            {
                passportPhoto = resizedImg

            }
            else if photo == photoType.license
            {
                licensePhoto = resizedImg

            }
//            if let base64String =  UIImageJPEGRepresentation(image, 0.1)?.base64EncodedString() {
//                // Upload base64String to your database
//                
//            }
            
            
//            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
            
            dismiss(animated: true, completion: nil)
        }
        
        
        
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 10 // Bool
    }
    
}
