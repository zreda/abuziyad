//
//  CreateAccountViewController.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 11/6/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import UIKit
import IQDropDownTextField

class CreateAccountViewController :UITableViewController ,  UITextFieldDelegate
{
    
    @IBOutlet weak var idNumberTF: DesignableUITextField!
    @IBOutlet weak var copyNumberTF: DesignableUITextField!
    @IBOutlet weak var dayExpiryIdTF: IQDropDownTextField!
    @IBOutlet weak var monthExpiryIdTF: IQDropDownTextField!
    @IBOutlet weak var yearExpiryIdTF: IQDropDownTextField!
    @IBOutlet weak var fnameTF: DesignableUITextField!
    @IBOutlet weak var secNameTF: DesignableUITextField!
    @IBOutlet weak var lnameTF: DesignableUITextField!
    @IBOutlet weak var phoneNumberTF: DesignableUITextField!
    @IBOutlet weak var mobileNumberTF: DesignableUITextField!
    @IBOutlet weak var nationalityTF: IQDropDownTextField!
    @IBOutlet weak var licenseNumberTF: DesignableUITextField!
    @IBOutlet weak var addressTF: DesignableUITextField!
    @IBOutlet weak var dayExpiryLicenseneTF: IQDropDownTextField!
    @IBOutlet weak var monthExpiryLicenseneTF: IQDropDownTextField!
    @IBOutlet weak var yearExpiryLicenseneTF: IQDropDownTextField!
    @IBOutlet weak var emailTF: DesignableUITextField!
    @IBOutlet weak var verifyEmailTF: DesignableUITextField!
    @IBOutlet weak var passwordTF: DesignableUITextField!
    @IBOutlet weak var confirmPasswordTF: DesignableUITextField!
    
    var selectedNationalityIndex:Int = -1
    var nationalityList:[NationalityModelResponse] = []
    var nationalities:[String] = []
    
   
    
    let daysArray:[String] = {

        if let url = Bundle.main.url(forResource:"Days", withExtension: "plist") {
            if let strArr = NSMutableArray(contentsOf:url) as? [String] {
                return strArr
            }
            return []
        }
        return []
    }()

    
    let monthsArray:[String] = {
        
        if let url = Bundle.main.url(forResource:"ArabicMonths", withExtension: "plist") {
            if let strArr = NSMutableArray(contentsOf:url) as? [String] {
                return strArr
            }
            return []
        }
        return []
    }()
    
    let yearsArray:[String] = {
        
        if let url = Bundle.main.url(forResource:"Years", withExtension: "plist") {
            if let strArr = NSMutableArray(contentsOf:url) as? [String] {
                return strArr
            }
            return []
        }
        return []
    }()
    
    

    @IBOutlet weak var haveAccount: UILabel!
    
    override func viewDidLoad() {
        getNationalities()
        self.setupView()

    }
    private func setupView()
    {
        hideKeyboard()
        title = "Register".localized()
        let haveAccountTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CreateAccountViewController.haveAccountTapped))
        
        
        
        var daysArray: [String] = []
        if let url = Bundle.main.url(forResource:"Days", withExtension: "plist") {
            if let englishFromPlist = NSMutableArray(contentsOf:url) as? [String] {
                for myEnglish in englishFromPlist {
                    daysArray.append(myEnglish)
                }
            }
        }
        
        self.dayExpiryIdTF.itemList = daysArray
        self.dayExpiryLicenseneTF.itemList = daysArray

        self.monthExpiryIdTF.itemList = monthsArray
        self.monthExpiryLicenseneTF.itemList = monthsArray

        self.yearExpiryIdTF.itemList = yearsArray
        self.yearExpiryLicenseneTF.itemList = yearsArray
        haveAccount.addGestureRecognizer(haveAccountTap)
        
        
        idNumberTF.delegate = self
        licenseNumberTF.delegate = self
        mobileNumberTF.delegate = self
        phoneNumberTF.delegate = self


    }
    
    /**
     Initialize an instance of About Company View Controller
     - returns: AboutViewController Object
     */
    public static func create() -> CreateAccountViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.authSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! CreateAccountViewController
    }
    @IBAction func registerBtnTapped(_ sender: Any) {
       
        validateUserDataEntry()

    }
    
    private func validateUserDataEntry()
    {

        if idNumberTF.text == ""
        {
            Helper.showFloatAlert(title: "please enter your id number".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        else if (idNumberTF.text?.count)! < 10
        {
            Helper.showFloatAlert(title: "id number should be 10 numbers".localized(), subTitle: "", type: Constants.AlertType.AlertError)
  
        }
        else if copyNumberTF.text == ""
        {
            Helper.showFloatAlert(title: "please enter your copy number".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
            
        else if dayExpiryIdTF.selectedRow == -1
        {
            Helper.showFloatAlert(title: "please select ID expiry  day ".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        else if monthExpiryIdTF.selectedRow == -1
        {
            Helper.showFloatAlert(title: "please select ID expiry  month ".localized(), subTitle: "", type: Constants.AlertType.AlertError)

            
        }
        else if yearExpiryIdTF.selectedRow == -1
        {
            Helper.showFloatAlert(title: "please select ID expiry year ".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        else if fnameTF.text == ""
        {
            Helper.showFloatAlert(title: "please enter your first name".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        else if secNameTF.text == ""
        {
            Helper.showFloatAlert(title: "please enter your second name ".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        else if lnameTF.text == ""
        {
            Helper.showFloatAlert(title: "please enter your last name".localized(), subTitle: "", type: Constants.AlertType.AlertError)

            
        }
        else if phoneNumberTF.text == ""
        {
            Helper.showFloatAlert(title: "please enter your phone number".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        else if (phoneNumberTF.text?.count)! < 10
        {
            Helper.showFloatAlert(title: "mobile number must be 10 numbers".localized(), subTitle: "", type: Constants.AlertType.AlertError)
            
        }
        else if mobileNumberTF.text == ""
        {
            Helper.showFloatAlert(title: "please enter your mobile number".localized(), subTitle: "", type: Constants.AlertType.AlertError)

            
        }
        else if (mobileNumberTF.text?.count)! < 10
        {
            Helper.showFloatAlert(title: "mobile number must be 10 numbers".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        else if nationalityTF.selectedRow == -1
        {
            Helper.showFloatAlert(title: "please select your nationality".localized(), subTitle: "", type: Constants.AlertType.AlertError)

            
        }
        else if licenseNumberTF.text == ""
        {
            Helper.showFloatAlert(title: "please enter your licensense number".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        else if addressTF.text == ""
        {
            Helper.showFloatAlert(title: "please enter your address".localized(), subTitle: "", type: Constants.AlertType.AlertError)

            
        }
        else if dayExpiryLicenseneTF.selectedRow == -1
        {
            Helper.showFloatAlert(title: "please select expiry licensense day ".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        else if monthExpiryLicenseneTF.selectedRow == -1
        {
            Helper.showFloatAlert(title: "please select expiry licensense month ".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        else if yearExpiryLicenseneTF.selectedRow == -1
        {
            Helper.showFloatAlert(title: "please select expiry licensense year ".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        else if emailTF.text == ""
        {
            Helper.showFloatAlert(title: "please enter your e-mail ".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        else if !Helper.isValidEmail(mail_address: emailTF.text!)
        {
            Helper.showFloatAlert(title: "please enter a vaild mail".localized(), subTitle: "", type: Constants.AlertType.AlertError)
            
        }
           
        else if verifyEmailTF.text == ""
        {
            Helper.showFloatAlert(title: "please enter verfiry your e-mail ".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        
        else if passwordTF.text == ""
        {
            Helper.showFloatAlert(title: "please enter your password".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        else if confirmPasswordTF.text == ""
        {
            Helper.showFloatAlert(title: "please  confirm your password".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        
        else if passwordTF.text != confirmPasswordTF.text
        {
            Helper.showFloatAlert(title: "confirm password not matched with password ".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
      
        else
        {
            let nationalityCode = self.nationalityList[nationalityTF.selectedRow].ntnltyCode!
            
           
//            let dateExpiry = (daysArray[dayExpiryIdTF.selectedRow] + "/" + monthsArray[monthExpiryIdTF.selectedRow] + "/" + yearsArray[yearExpiryIdTF.selectedRow]) as NSString

            let dateExpiry = daysArray[dayExpiryIdTF.selectedRow] + "/" + monthsArray[monthExpiryIdTF.selectedRow] + "/" + yearsArray[yearExpiryIdTF.selectedRow]
            
            let dateLicense = daysArray[dayExpiryLicenseneTF.selectedRow] + "/" + monthsArray[monthExpiryLicenseneTF.selectedRow] + "/" + yearsArray[yearExpiryLicenseneTF.selectedRow]

            
            let authData = RegisterRequestModel(addressA : addressTF.text!,copyNo : copyNumberTF.text! ,dayIdExpiryDt : dateExpiry,dayLcnsEndDate : dateLicense, email : emailTF.text! , firstName : fnameTF.text! ,lastName : lnameTF.text! , licenseNo : licenseNumberTF.text! , mobileNumber : mobileNumberTF.text! , ntnltyCode : nationalityCode , password : passwordTF.text! , phoneNumber : phoneNumberTF.text! , secondName : secNameTF.text! ,username : idNumberTF.text! )
            
            
            AuthenticationManager().register(auth: authData, completion: { (response, error) in

                if response != nil
                {
                    if response?.id == nil
                    {
                        Helper.showFloatAlert(title: "Error in Registeration , please verfity you data".localized(), subTitle: "", type: Constants.AlertType.AlertError)

                    }
                    else
                    {
                        
                        self.login(username: self.idNumberTF.text!, password: self.passwordTF.text!)
                    }
                }
                else
                {
                    Helper.showFloatAlert(title: "Something went wrong!".localized(), subTitle: "", type: Constants.AlertType.AlertError)

                }
            })
        }

        
    }
    
    
    
    private func login(username:String , password:String)
    {
        
        
        AuthenticationManager().login(username: username, password: password, completion: { (response, error) in
            
            if (error == nil)
            {
                if response?.msg != nil
                {
                    Helper.showFloatAlert(title: (response?.msg!)!, subTitle: "", type: Constants.AlertType.AlertError)
                }
                else
                {
                    
                    self.navigationController?.popViewController(animated: true)
                    Helper.saveObjectDefault(key: Constants.userDefault.userData, value:response?.data ?? "")
                }
            }
            else
            {
                
                Helper.showFloatAlert(title: "Something went wrong!".localized(), subTitle: "", type: Constants.AlertType.AlertError)
                
            }
            
        })
    }
    
    @objc private func haveAccountTapped() {
        
        
        self.navigationController?.popViewController(animated: true)
        
    }
}

extension CreateAccountViewController
{
    
    
    func getNationalities()
    {
        AuthenticationManager().getAllNationality { (response, error) in
            
            if response != nil
            {
                 self.nationalities = []
                self.nationalityList = response!
                
                for (_,nationality) in (self.nationalityList.enumerated())
                {
                    self.nationalities.append((nationality.ntnlty ?? "")!)
                }
                 self.nationalityTF.itemList = self.nationalities

            }
            else
            {
                Helper.showFloatAlert(title: "Something went wrong!".localized(), subTitle: "", type: Constants.AlertType.AlertError)

            }
        }
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 10 // Bool
    }
}
