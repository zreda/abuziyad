//
//  UpdatePasswordViewController.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 12/8/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class UpdatePasswordViewController: UIViewController {
    
    @IBOutlet weak var oldPassword: DesignableUITextField!
    
    @IBOutlet weak var newPassword: DesignableUITextField!
    
    @IBOutlet weak var confirmPassword: DesignableUITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
    }
    
    /**
     Initialize an instance of About Company View Controller
     - returns: AboutViewController Object
     */
    public static func create() -> UpdatePasswordViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.authSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! UpdatePasswordViewController
    }
    
    @IBAction func resetPasswordBtnTapped(_ sender: Any)
    {
        let userData = Helper.getObjectDefault(key: Constants.userDefault.userData) as! UserDataModel
        
        if oldPassword.text == ""
        {
         Helper.showFloatAlert(title: "please enter old password".localized(), subTitle: "", type: Constants.AlertType.AlertError)
        }
        else if newPassword.text == ""
        {
            Helper.showFloatAlert(title: "please enter new password".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        else if confirmPassword.text == ""
        {
            Helper.showFloatAlert(title: "please confirm old password".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        else if newPassword.text != confirmPassword.text
        {
            Helper.showFloatAlert(title: "new password must be same as confirm password".localized(), subTitle: "", type: Constants.AlertType.AlertError)

        }
        else
        {
            AuthenticationManager().resetPassword(auth: ResetRequestModel(conPassword: newPassword.text!, password: confirmPassword.text!, userId: String(describing:userData.iD), username: userData.userLogin!)) { (response, error) in
                
                if error == nil
                {
                    Helper.showFloatAlert(title:  "your password updated successfully".localized(), subTitle: "", type: Constants.AlertType.AlertSuccess)
                    
                    self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    Helper.showFloatAlert(title: "Something went wrong!".localized(), subTitle: "", type: Constants.AlertType.AlertError)
                    
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
