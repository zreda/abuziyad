//
//  SiginViewController.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 11/6/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class SiginViewController :UIViewController
{
    
    @IBOutlet weak var idNumber: DesignableUITextField!
    @IBOutlet weak var password: DesignableUITextField!
    @IBOutlet weak var forgetPassword: UILabel!
    @IBOutlet weak var registerNow: UILabel!
    override func viewDidLoad() {
        
        title = "login_title".localized()
        hideKeyboard()
        
        let forgetPasswordTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SiginViewController.forgetPasswordTapped))
        let registerNowTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SiginViewController.registerNowTapped))
        
        forgetPassword.addGestureRecognizer(forgetPasswordTap)
        registerNow.addGestureRecognizer(registerNowTap)
        
    }
    
    /**
     Initialize an instance of About Company View Controller
     - returns: AboutViewController Object
     */
    public static func create() -> SiginViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.authSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! SiginViewController
    }
    
    @IBAction func signinBtnTapped(_ sender: Any) {
        
        if idNumber.text != "" && password.text != ""
        {
            AuthenticationManager().login(username: idNumber.text!, password: password.text!, completion: { (response, error) in
                
                if (error == nil)
                {
                    if response?.msg != nil
                    {
                        Helper.showFloatAlert(title: (response?.msg!)!, subTitle: "", type: Constants.AlertType.AlertError)
                    }
                    else
                    {
                        
                        self.navigationController?.popViewController(animated: true)
                        Helper.saveObjectDefault(key: Constants.userDefault.userData, value:response?.data ?? "")
                    }
                }
                else
                {
                    
                    Helper.showFloatAlert(title: "Something went wrong!".localized(), subTitle: "", type: Constants.AlertType.AlertError)

                }
        
            })
        }
        else if idNumber.text == ""
        {
            Helper.showFloatAlert(title: "idNumberEmpty".localized(), subTitle: "", type: Constants.AlertType.AlertError)
            
        }
        else if password.text == ""
        {
            Helper.showFloatAlert(title: "passwordEmpty".localized(), subTitle: "", type: Constants.AlertType.AlertError)
            
        }
        
    }
    
    @objc private func forgetPasswordTapped() {
        
        
        self.navigationController?.pushViewController(ForgetPasswordViewController.create(), animated: true)
        
    }
    
    @objc private func registerNowTapped() {
        
        self.navigationController?.pushViewController(CreateAccountViewController.create(), animated: true)
        
    }
}
