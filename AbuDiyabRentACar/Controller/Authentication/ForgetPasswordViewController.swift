//
//  ForgetPasswordViewController.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 11/6/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import UIKit

class ForgetPasswordViewController :UIViewController
{
    
    @IBOutlet weak var emailTF: DesignableUITextField!
    override func viewDidLoad() {
        hideKeyboard()
    }
    
    /**
     Initialize an instance of About Company View Controller
     - returns: AboutViewController Object
     */
    public static func create() -> ForgetPasswordViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.authSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! ForgetPasswordViewController
    }
    @IBAction func restPasswordBtnTapped(_ sender: Any) {
        
        if emailTF.text != "" && Helper.isValidEmail(mail_address: emailTF.text!)
        {
            
            AuthenticationManager().forgetPassword(email: emailTF.text!, completion: { (response, error) in
                    if response != nil
                    {
                       if response?.Error == nil
                       {
                        self.navigationController?.popViewController(animated: true)
                        Helper.showFloatAlert(title:  "your reset link sent to your email".localized(), subTitle: "", type: Constants.AlertType.AlertSuccess)


                        }
                        else
                       {
                            Helper.showFloatAlert(title: "invalid email".localized(), subTitle: "", type: Constants.AlertType.AlertError)
                        }
                    }
                    else
                    {
                        Helper.showFloatAlert(title: "Something went wrong!".localized(), subTitle: "", type: Constants.AlertType.AlertError)

                    }
            })
        }
        else
        {
            if emailTF.text == ""
            {
                Helper.showFloatAlert(title: "please enter your mail".localized(), subTitle: "", type: Constants.AlertType.AlertError)

            }
            else if !Helper.isValidEmail(mail_address: emailTF.text!)
            {
                Helper.showFloatAlert(title: "please enter a vaild mail".localized(), subTitle: "", type: Constants.AlertType.AlertError)

            }
            
        }

    }
}
