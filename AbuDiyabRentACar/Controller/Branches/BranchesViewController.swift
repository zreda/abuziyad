//
//  BranchesViewController.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 11/11/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import IQDropDownTextField

class BranchesViewController: UIViewController, IQDropDownTextFieldDelegate {
    
    // MARK: - Properties
    
    // MARK: - Private Instance Outlets
    
    @IBOutlet fileprivate  weak var branchesMapView: MKMapView!
    
    // MARK: - Private Instance Variables
    
    fileprivate var locationManager: CLLocationManager = CLLocationManager()
    @IBOutlet weak var filterView: UIView!
    @IBOutlet var inputFilters: [IQDropDownTextField]!
    
    fileprivate var currLoc: CLLocationCoordinate2D!
    
    fileprivate var branches: [AZBranchesModel] = []
    
    var selectedCity = ""
    var selectedBranch = ""
    
    var branchsItems:[AZBranch] = []
    var selectedCityObj: AZBranchesModel?
    var brachItemStr:[String] = []
    var branchToShow:[AZBranch] = []
    // MARK: - Methods
    
    // MARK: - Init
    
    /**
     Initialize an instance of Company Branches View Controller
     - returns: BranchesViewController Object
     */
    public static func create() -> BranchesViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! BranchesViewController
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let toolbar = UIToolbar()
        toolbar.barStyle = .blackTranslucent
        toolbar.sizeToFit()
        let buttonflexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let buttonDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.doneClicked))
        toolbar.items = [buttonflexible, buttonDone]
        
        
        for item in inputFilters {
            item.isOptionalDropDown = false
            item.inputAccessoryView = toolbar
        }
        
        let branchesRequest = AZBranchesRequest()
        branchesRequest.getBranchesList()
        title = "title_text".localized()
        inputFilters[0].delegate = self
        inputFilters[1].delegate = self
        
        branchesRequest.didGetBranchesList = { [weak self] (branches, error) -> Void in
            if let error = error {
                Helper.showFloatAlert(title: error.localizedDescription, subTitle: "", type: Constants.AlertType.AlertError)
            } else {
                if let branches = branches {
                    var branchesList:[String] = ["All Branches".localized()]
                    var citiesList: [String] = ["All Cities".localized()]
                    
                    for b in branches {
                        citiesList.append(b.city)
                        for c in b.branch {
                            branchesList.append(c.name)
                            self?.branchsItems.append(c)
                            
                        }
                    }
                    self?.inputFilters[0].itemList = citiesList
                    self?.inputFilters[1].itemList = branchesList
                    self?.brachItemStr = branchesList
                    self?.branches = branches
                    
                    self?.currLoc = CLLocationCoordinate2D(latitude: Double(branches[0].latitude) ?? 18.234432, longitude: Double(branches[0].longitude) ?? 42.657329)
                    let region = MKCoordinateRegionMake(self!.currLoc, MKCoordinateSpan(latitudeDelta: 0.6, longitudeDelta: 0.6))
                    self?.branchesMapView.setRegion(region, animated: true)
                    DispatchQueue.main.async {
                        self?.loadMap(arr:(self?.branchsItems)!)
                        self?.branchesMapView.reloadInputViews()
                    }
                }
            }
        }
    }
    
    func doneClicked(_ button: UIBarButtonItem) {
        view.endEditing(true)
    }
    
    
    
    
    @IBAction func inlineFilterAction() {
        filterView.isHidden = true
        
        selectedCity = inputFilters[0].selectedItem! == "All Cities".localized() ? "" : inputFilters[0].selectedItem!
        selectedBranch = inputFilters[1].selectedItem! == "All Branches".localized() ? "" : inputFilters[1].selectedItem!
        loadMap(arr:branchToShow)
        //branchesMapView.reloadInputViews()
        
        
    }
    
    @IBAction private func filterAction() {
        filterView.isHidden = false
    }
    
    
    func textField(_ textField: IQDropDownTextField, didSelectItem item: String?) {
        
        if selectedCity != item && textField == inputFilters[0]
        {
            selectedCityObj = branches[inputFilters[0].selectedRow-1 == -1 ? 0 : inputFilters[0].selectedRow-1]
            self.branchsItems = selectedCityObj!.branch
            branchToShow = self.branchsItems
            selectedCity = item!
            self.brachItemStr = []
            for (_,item) in branchsItems.enumerated()
            {
                brachItemStr.append(item.name)
            }
            brachItemStr.insert("All Branches".localized(), at: 0)
            inputFilters[1].itemList = brachItemStr
            branchToShow = self.branchsItems

        }
        
        if  selectedBranch != item && textField == inputFilters[1]
        {
            selectedBranch = item!
            if inputFilters[1].selectedRow-1 != -1 {
                branchToShow  = [self.branchsItems[inputFilters[1].selectedRow-1 == -1 ? 0 : inputFilters[1].selectedRow-1]]
            }
            else
            {
                branchToShow = self.branchsItems
            }
        }
    }
}


extension BranchesViewController: MKMapViewDelegate {
    
    
    fileprivate func loadMap(arr:[AZBranch]) {
        
       
        
        let allAnnotations = self.branchesMapView.annotations
        self.branchesMapView.removeAnnotations(allAnnotations)
        
        if let sC = selectedCityObj {
            
            currLoc = CLLocationCoordinate2D(latitude: Double(sC.latitude) ?? 18.234432, longitude: Double(sC.longitude) ?? 42.657329)
            let region = MKCoordinateRegionMake(currLoc, MKCoordinateSpan(latitudeDelta: 0.6, longitudeDelta: 0.6))
            branchesMapView.setRegion(region, animated: true)
        }
        
        for branch in arr {
            
            let location = CLLocationCoordinate2D(latitude: Double(branch.latitude)!, longitude: Double(branch.longitude)!)
            let pointAnnotation = CustomPointAnnotation()
            pointAnnotation.pinCustomImageName = "contacts_map"
            pointAnnotation.coordinate = location
            pointAnnotation.title = branch.name
            pointAnnotation.subtitle = branch.phone1
            let pinAnnotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: "pin")
            branchesMapView.addAnnotation(pinAnnotationView.annotation!)
        }
        
    }
    
    
    //MARK: - Custom Annotation
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let reuseIdentifier = "pin"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
        } else {
            annotationView?.annotation = annotation
        }
        
        annotationView?.canShowCallout = true
        let customPointAnnotation = annotation as! CustomPointAnnotation
        annotationView?.image = UIImage(named: customPointAnnotation.pinCustomImageName)
        annotationView?.image = #imageLiteral(resourceName: "contacts_map")
        
        return annotationView
    }
}



class CustomPointAnnotation: MKPointAnnotation {
    
    var pinCustomImageName: String!
}


