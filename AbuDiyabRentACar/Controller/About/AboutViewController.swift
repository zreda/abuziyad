//
//  AboutViewController.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 11/11/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    @IBOutlet private weak var aboutTextView: UITextView!
    
    
    
    // MARK: - Methods
    
    // MARK: - Init
    
    /**
     Initialize an instance of About Company View Controller
     - returns: AboutViewController Object
     */
    public static func create() -> AboutViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! AboutViewController
    }
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "title_text".localized()
        aboutTextView.text = "about_text".localized()
    }


}
