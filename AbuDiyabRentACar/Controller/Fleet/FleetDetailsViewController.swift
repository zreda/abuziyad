//
//  FleetDetailsViewController.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 12/5/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class FleetDetailsViewController: UIViewController {

    @IBOutlet weak var viewTitle: UILabel!
    
    @IBOutlet weak var logoImg: UIImageView!
    
    @IBOutlet weak var carName: UILabel!
    
    @IBOutlet weak var carDetailsTB: UITableView!
    @IBOutlet weak var carPrice: UILabel!
    static var fleetItem :FleetModelRespopnse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewTitle.text = FleetDetailsViewController.fleetItem?.title
        
        
        title = "title_text".localized()
        carPrice.text =  FleetDetailsViewController.fleetItem?.regularPrice ?? "" + " SAR".localized()
        carName.text = FleetDetailsViewController.fleetItem?.manufactureName
        logoImg.sd_setImage(with: URL(string: (FleetDetailsViewController.fleetItem?.manufactureLogo ?? "")!), placeholderImage: #imageLiteral(resourceName: "default_img"))

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /**
     Initialize an instance of About Company View Controller
     - returns: AboutViewController Object
     */
    public static func create() -> FleetDetailsViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.fleetSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! FleetDetailsViewController
    }
    @IBAction func bookNowBtnTapped(_ sender: Any) {
        
        if Helper.isKeyPresentInUserDefaults(key: Constants.userDefault.userData)
        {
            let vc = BookingTableViewController.create()
            vc.fleetItem = FleetDetailsViewController.fleetItem
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            self.navigationController?.pushViewController(SiginViewController.create(), animated: true)
            
        }
    }
}

extension FleetDetailsViewController : UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = carDetailsTB.dequeueReusableCell(withIdentifier: "cell") 
        if indexPath.row == 0
        {
            cell?.textLabel?.text = "Manfacture Year".localized()
            cell?.detailTextLabel?.text = FleetDetailsViewController.fleetItem!.manufactureYear
        }
        else if indexPath.row == 1
        {
            cell?.textLabel?.text = "Engine Capacity".localized()
            cell?.detailTextLabel?.text = FleetDetailsViewController.fleetItem!.engineCapacity


        }
        else if indexPath.row == 2
        {
            cell?.textLabel?.text = "Number of Cylinders".localized()
            cell?.detailTextLabel?.text = FleetDetailsViewController.fleetItem!.cylinders

        }
        else if indexPath.row == 3
        {
            cell?.textLabel?.text = "Horsepower".localized()
            cell?.detailTextLabel?.text = FleetDetailsViewController.fleetItem!.horsepower

            
        }
        else if indexPath.row == 4
        {
            cell?.textLabel?.text = "Number of doors".localized()
            cell?.detailTextLabel?.text = FleetDetailsViewController.fleetItem!.doors


        }
        else if indexPath.row == 5
        {
            cell?.textLabel?.text = "Number of seats".localized()
            cell?.detailTextLabel?.text = FleetDetailsViewController.fleetItem!.seats

        }
        else if indexPath.row == 6
        {
            cell?.textLabel?.text = "fuel".localized()
            cell?.detailTextLabel?.text = FleetDetailsViewController.fleetItem!.fuelType

        }
        else if indexPath.row == 7
        {
            cell?.textLabel?.text = "Drive Type".localized()
            cell?.detailTextLabel?.text = FleetDetailsViewController.fleetItem!.driveType

            
        }
        else if indexPath.row == 8
        {
            cell?.textLabel?.text = "Wheel Size".localized()
            cell?.detailTextLabel?.text = FleetDetailsViewController.fleetItem!.wheelsSize

        }
        else if indexPath.row == 9
        {
            cell?.textLabel?.text = "Car Transmission".localized()
            cell?.detailTextLabel?.text = FleetDetailsViewController.fleetItem!.carTransmission

        }
        else if indexPath.row == 10
        {
            cell?.textLabel?.text = "Remote Control".localized()
            cell?.detailTextLabel?.text = FleetDetailsViewController.fleetItem!.remoteControl

        }
        return cell ?? UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11

    }
    
   
    
}
