//
//  BookingTableViewController.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 12/8/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import SDWebImage

class BookingTableViewController: UITableViewController {
    
    @IBOutlet weak var carImg: UIImageView!
    @IBOutlet weak var carType: UILabel!
    @IBOutlet weak var priceBefore: UILabel!
    @IBOutlet weak var priceAfter: UILabel!
    @IBOutlet weak var numberOfDays: DesignableUITextField!
    var fleetItem :FleetModelRespopnse?
    
    @IBOutlet weak var total: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if fleetItem?.carSlider?.count != 0 &&  fleetItem?.carSlider != nil
        {
            carImg.sd_setImage(with: URL(string: (fleetItem?.carSlider?[0] ?? "")) , placeholderImage: #imageLiteral(resourceName: "default_img"))
        }
        else
        {
            carImg.sd_setImage(with: URL(string: (fleetItem?.thumbnail ?? "")) , placeholderImage: #imageLiteral(resourceName: "default_img"))
        }
        carType.text = fleetItem?.title
        priceBefore.text =  ((fleetItem?.regularPrice !=  "" && fleetItem?.regularPrice != nil) ? (fleetItem?.regularPrice)! : "0")! + " SAR".localized()
        
        priceAfter.text = ((fleetItem?.salePrice !=  "" && fleetItem?.salePrice != nil) ? (fleetItem?.salePrice)! : "0")! + " SAR".localized()
        
        total.text =  priceAfter.text! 
        self.hideKeyboard()
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public static func create() -> BookingTableViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.fleetSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! BookingTableViewController
    }
    
    @IBAction func numberOfDaysChanged(_ sender: Any) {
        
        
        let res = (NSInteger(numberOfDays.text ?? "0") != nil ? NSInteger(numberOfDays.text ?? "0")  : 0)! * (NSInteger(priceAfter.text!.replacingOccurrences(of: " SAR".localized(), with: "", options: .literal, range: nil) ?? "0") != nil ? NSInteger(priceAfter.text!.replacingOccurrences(of: " SAR".localized(), with: "", options: .literal, range: nil) ?? "0")  : 0)!
        
        total.text = String(describing:res) + " SAR".localized()
        
    }
    @IBAction func nextBtnTapped(_ sender: Any)
    {
        if total.text!.hasPrefix("0")
        {
            Helper.showAlert(type: Constants.AlertType.AlertError, title: "Error", subTitle: "Sorry you can not complete booking this car due to proce error".localized(), closeTitle: "ok".localized())
        }
        else
        {
            let vc = PaymentTableViewController.create()
            vc.carId = String(fleetItem?.id as! Int)
            vc.numOfDays = numberOfDays.text!
            vc.userID  = String(((Helper.getObjectDefault(key: Constants.userDefault.userData) as! UserDataModel).iD as! Int))
        
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
