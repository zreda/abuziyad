//
//  FilterTableViewController.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 12/5/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class FilterTableViewController: UIViewController {
    
    @IBOutlet weak var filterTB: UITableView!
    var filterArr :[FilterModelResponse] = []
    @IBOutlet weak var noFilterFound: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getFilter()
        title = "Car Types".localized()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     Initialize an instance of Company Branches View Controller
     - returns: BranchesViewController Object
     */
    public static func create() -> FilterTableViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.fleetSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! FilterTableViewController
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension FilterTableViewController :UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = filterTB.dequeueReusableCell(withIdentifier: "filter_cell") as! UITableViewCell
        if LanguageManger.shared.currentLang == "en"
        {
            cell.textLabel?.text = filterArr[indexPath.row].machineName
        }
        else
        {
            cell.textLabel?.text = filterArr[indexPath.row].category
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return filterArr.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        FleetViewController.catId = String(describing:filterArr[indexPath.row].id as! Int)
        self.navigationController?.popViewController(animated: true)
        
    }
    func getFilter()
    {
        FleetServiceManager().getFilters { (response, error) in
            self.filterArr  = []
            if response != nil
            {
                if response?.count == 0
                {
                    self.noFilterFound.isHidden = false
                    
                }
                else
                {
                    
                    self.filterArr = response!
                    self.filterArr.insert(FilterModelResponse(category: "All".localized(), id: 0, machineName: "All".localized()), at: 0)
                    self.noFilterFound.isHidden = true
                }
            }
            else
            {
                self.noFilterFound.isHidden = false
                Helper.showFloatAlert(title: "Something went wrong!".localized(), subTitle: "", type: Constants.AlertType.AlertError)
                
                
            }
            self.filterTB.reloadData()

        }
    }
}
