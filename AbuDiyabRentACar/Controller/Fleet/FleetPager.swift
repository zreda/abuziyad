//
//  FleetPager.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 12/8/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import UIKit
class FleetPager: UIView, UIScrollViewDelegate {
    
    @IBOutlet weak var scroll: UIScrollView!
    
    var carImages = FleetDetailsViewController.fleetItem?.carSlider as? [String] ?? []
    override func draw(_: CGRect) {
        
        scroll.isHidden = false
        scroll.delegate = self
        
        
        for item in carImages {
            self.scroll.auk.show(url: item)
            
        }
        
        
        scroll.auk.settings.placeholderImage = #imageLiteral(resourceName: "img3")
        scroll.auk.settings.errorImage = #imageLiteral(resourceName: "default_img")
        
        scroll.auk.startAutoScroll(delaySeconds: 3)
        scroll.auk.settings.contentMode = .scaleAspectFill
        scroll.auk.settings.pageControl.marginToScrollViewBottom = 50
        scroll.auk.settings.pageControl.currentPageIndicatorTintColor = UIColor.red
    }
    
    
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidScroll(_: UIScrollView)
    {
        
    }
    
    @IBAction func onScrollViewTapped(_: AnyObject)
    {
        
    }
}
