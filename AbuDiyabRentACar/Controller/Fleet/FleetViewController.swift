//
//  FleetViewController.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 11/30/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class FleetViewController: UIViewController {
    
    @IBOutlet weak var fleetCollectionView: UICollectionView!
    var fleetArr:[FleetModelRespopnse] = []
    @IBOutlet weak var noFleetLB: UILabel!
    static var catId:String = ""
    override func viewDidLoad() {
        
        initView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        getFleets()
        
    }
    
    func initView()
    {
       
        title = "title_text".localized()
        let nib = UINib(nibName: "FleetCell", bundle: nil)
        fleetCollectionView?.register(nib, forCellWithReuseIdentifier: "fleet_cell")
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 1.0
        layout.minimumLineSpacing = 1.0
        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .phone:
            layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            
            layout.itemSize = CGSize(width: (UIScreen.main.bounds.width - 20)/2, height: fleetCellHeight)
            
            break
        case .pad:
            layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            layout.itemSize = CGSize(width: fleetCellWidth, height: fleetCellHeight)
            
            break
            
        default:
            layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            
            layout.itemSize = CGSize(width: UIScreen.main.bounds.width - 20, height: 350.0)
            
            break
        }
        
        fleetCollectionView!.collectionViewLayout = layout
        
    }
    
    @IBAction func filterBtnTapped(_ sender: Any)
    {
        self.navigationController?.pushViewController(FilterTableViewController.create(), animated: true)
    }
    
    /**
     Initialize an instance of Company Branches View Controller
     - returns: BranchesViewController Object
     */
    public static func create() -> FleetViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.fleetSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! FleetViewController
    }
    
}

extension FleetViewController :UICollectionViewDelegate , UICollectionViewDataSource
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return fleetArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = fleetCollectionView.dequeueReusableCell(withReuseIdentifier: "fleet_cell", for: indexPath) as! FleetCellItem
        
        
        cell.configureCell(item: fleetArr[indexPath.row])
        cell.detailBtn.addTarget(self, action: #selector(FleetViewController.detailBtnTapped), for: .touchUpInside)
        cell.detailBtn.tag = indexPath.row
        cell.bookBtn.addTarget(self, action: #selector(FleetViewController.bookNowBtnTapped), for: .touchUpInside)
        cell.bookBtn.tag = indexPath.row

        return cell
    }
    
    func bookNowBtnTapped(sender : UIButton!)
    {
        if Helper.isKeyPresentInUserDefaults(key: Constants.userDefault.userData)
        {
            let vc = BookingTableViewController.create()
            vc.fleetItem = fleetArr[sender.tag]
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            self.navigationController?.pushViewController(SiginViewController.create(), animated: true)

        }
    }
    
    func detailBtnTapped(sender : UIButton!)
    {
        let vc = FleetDetailsViewController.create()
        FleetDetailsViewController.fleetItem = fleetArr[sender.tag]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func getFleets()
    {
        
        FleetServiceManager().getFleets(cat_id: FleetViewController.catId, completion:  { (response, error) in
            self.fleetArr = []
            if response != nil
            {
                if response?.count == 0
                {
                    self.noFleetLB.isHidden = false
                }
                else
                {
                    self.noFleetLB.isHidden = true
                    self.fleetArr = response!
                }
            }
            else
            {
                self.noFleetLB.isHidden = false
                Helper.showFloatAlert(title: "Something went wrong!".localized(), subTitle: "", type: Constants.AlertType.AlertError)
                
            }
            self.fleetCollectionView.reloadData()

        })
        
    }
    
}
