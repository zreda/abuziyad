//
//  HomeOffersCell.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 12/5/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import SDWebImage
import ChainableAnimations

class HomeOffersCell: UICollectionViewCell {
    
    @IBOutlet weak var offerLabel: UILabel!
    @IBOutlet weak var offerImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    private var id: Int!
    internal var book: ((Int) -> ())?
    @IBOutlet weak var offerView: RoundRectView!
    
    @IBOutlet weak var offerTrailingConstraint: NSLayoutConstraint!
    var animated =  false
 
    func setupView(_ data: AZHomeOffersModel) {
        
      
        offerImageView.sd_setImage(with: URL(string: data.image)!, placeholderImage: #imageLiteral(resourceName: "default_img"))
        titleLabel.text = data.title
        descriptionLabel.text = data.descriptionField
        id = data.id
        
        if !animated {
            let chAn = ChainableAnimator(view: offerView)
            chAn.move(x: 115).rotate(angle: 360.0).animate(t: 0.8, completion: {
                self.offerLabel.text = data.salePrice + " SAR".localized()
                if LanguageManger.shared.currentLang == "ar" {
                    self.offerTrailingConstraint.constant = 125
                }
                self.animated = true
            })
        }
        
        
    }
    
    @IBAction func book(_ sender: UIButton) {
        book?(id)
    }
}
