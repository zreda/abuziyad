//
//  HomeViewcontroller.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 11/6/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import SideMenu

class HomeViewcontroller: UITableViewController {
    
    @IBOutlet weak var homeOffersCollection: UICollectionView!
    
    var offers: [AZHomeOffersModel]?
    
    override func viewDidLoad() {
        
        Helper.setupSideMenue(nav: self.navigationController!)
        
        title = "title_text".localized()
        
        let homeManager = AZHomeRequest()
        homeManager.getHomeOffers()
        homeManager.didGetHomeOffers = { (offers, error) in
            
            if let error = error {
                debugPrint(error.localizedDescription)
                Helper.showFloatAlert(title: error.localizedDescription, subTitle: "", type: Constants.AlertType.AlertError)
            } else {
                self.offers = offers
                self.homeOffersCollection.reloadData()
            }
        }
    }
    public static func create() -> HomeViewcontroller {
        
        return UIStoryboard(name: Constants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewcontroller
    }
    
    
    @IBAction func sideMenuTapped(_ sender: Any) {
        
        if Helper.isKeyPresentInUserDefaults(key: Constants.userDefault.userData)
        {
            let menuLeftNavigationController = self.storyboard?.instantiateViewController(withIdentifier: "UserSideMenu") as! UISideMenuNavigationController
            
            
            if LanguageManger.shared.currentLang == "en"
            {
                
                menuLeftNavigationController.leftSide = true
                SideMenuManager.menuLeftNavigationController = menuLeftNavigationController
                
            }
            else
            {
                menuLeftNavigationController.leftSide = false
                SideMenuManager.menuRightNavigationController = menuLeftNavigationController
                
                
            }
            self.navigationController?.present(menuLeftNavigationController, animated: true, completion: nil)
        }
        else
        {
            
            let menuLeftNavigationController = self.storyboard?.instantiateViewController(withIdentifier: "GuestSideMenu") as! UISideMenuNavigationController
            
            if LanguageManger.shared.currentLang == "en"
            {
                
                menuLeftNavigationController.leftSide = true
                SideMenuManager.menuLeftNavigationController = menuLeftNavigationController
                
            }
            else
            {
                menuLeftNavigationController.leftSide = false
                SideMenuManager.menuRightNavigationController = menuLeftNavigationController
                
            }
            
            self.navigationController?.present(menuLeftNavigationController, animated: true, completion: nil)
            
        }
    }
    
    
    
}


extension HomeViewcontroller: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return offers != nil ? offers!.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeOffersCell", for: indexPath) as! HomeOffersCell
        cell.setupView(offers![indexPath.row])
        cell.book = { id in
            
            
            if Helper.isKeyPresentInUserDefaults(key: Constants.userDefault.userData)
            {
                let vc = BookingTableViewController.create()
                
                vc.fleetItem = FleetModelRespopnse(descriptionField: self.offers![indexPath.row].descriptionField, id: self.offers![indexPath.row].id, image: self.offers![indexPath.row].image, regularPrice: self.offers![indexPath.row].regularPrice, salePrice: self.offers![indexPath.row].salePrice, title: self.offers![indexPath.row].title)
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else
            {
                self.navigationController?.pushViewController(SiginViewController.create(), animated: true)
                
            }
            
        }
        return cell
    }
}
