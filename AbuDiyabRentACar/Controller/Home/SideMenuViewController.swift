//
//  SideMenuViewController.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 12/15/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    fileprivate lazy var arrItems: [[String: Any]] = [
        ["image": #imageLiteral(resourceName: "home"), "name" :  "home_text".localized()],
        ["image": #imageLiteral(resourceName: "about"), "name" :  "about_title_text".localized()],
        ["image": #imageLiteral(resourceName: "membership"), "name" :  "membership_main_text".localized()],
        ["image": #imageLiteral(resourceName: "media-center"), "name" :  "fleet_text".localized()],
        ["image": #imageLiteral(resourceName: "branch"), "name" :  "branches_text".localized()],
        ["image": #imageLiteral(resourceName: "contactus"), "name" :  "contact_us_title_text".localized()],
        ["image": #imageLiteral(resourceName: "settings"), "name" :  "settings_text".localized()]
    ]
    fileprivate var collapsed = true
    
    
    @IBAction private func loginBtnTapped(_ sender: Any) {
        self.navigationController?.pushViewController(SiginViewController.create(), animated: true)
    }
    
    @IBAction private func registerBtnTapped(_ sender: Any) {
        self.navigationController?.pushViewController(CreateAccountViewController.create(), animated: true)
    }
    
}

extension SideMenuViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
        cell.configureCell(arrItems[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var targetedViewController: UIViewController
        switch (indexPath.row, collapsed) {
            
        case (1, _):
            targetedViewController = AboutViewController.create()
        case (2, _):
            if collapsed {
                
                collapsed = false
                arrItems.insert([
                    "image": "nil", "name" :  "membership_title".localized()
                    ], at: 3)
                arrItems.insert([
                    "image": "nil", "name" :  "reward_title".localized()
                    ], at: 4)

                tableView.beginUpdates()
                tableView.insertRows(at: [IndexPath(row: 3, section: 0), IndexPath(row: 4, section: 0)], with: .top)
                tableView.endUpdates()
            } else {
                
                collapsed = true
                arrItems.remove(at: 3)
                arrItems.remove(at: 4)
                
                tableView.beginUpdates()
                tableView.deleteRows(at: [IndexPath(row: 3, section: 0), IndexPath(row: 4, section: 0)], with: .top)
                tableView.endUpdates()
            }
            return
            
        case (3, false):
            targetedViewController = MembershipViewController.create()
        case (3, true), (5, false):
            targetedViewController = FleetViewController.create()
        case (4, false):
            targetedViewController = RewardViewController.create()
        case (4, true), (6, false):
            targetedViewController = BranchesViewController.create()
        case (5, true), (7, false):
            targetedViewController = ContactUsViewController.create()
        case (6, true), (8, false):
            targetedViewController = SettingsTableViewController.create()
            
    
        default:
            return
        }
        navigationController?.pushViewController(targetedViewController, animated: true)
    }
            
}
