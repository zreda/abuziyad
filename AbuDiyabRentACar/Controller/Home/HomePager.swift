//
//  OFCNewsPager.swift
//  OrangeFootballClub
//
//  Created by Alaa Taher on 11/17/16.
//  Copyright © 2016 Alaa Taher. All rights reserved.
//

import UIKit
import Auk

class HomePager: UIView, UIScrollViewDelegate {

    @IBOutlet weak var scroll: UIScrollView!
   

    override func draw(_: CGRect) {
        
        scroll.isHidden = false
        scroll.delegate = self
        
        let homeManager = AZHomeRequest()
        homeManager.getHomeSliderImages()
        homeManager.didGetHomeSliderImages = { (imagesUrls, error) in
            
            if let error = error {
                debugPrint(error.localizedDescription)
                Helper.showFloatAlert(title: error.localizedDescription, subTitle: "", type: Constants.AlertType.AlertError)
            } else {
                for item in imagesUrls! {
                    if let img = item.sliderImage {
                        self.scroll.auk.show(url: img)
                    }
                }
            }
        }
    
        scroll.auk.settings.placeholderImage = #imageLiteral(resourceName: "default_img")
        scroll.auk.settings.errorImage = #imageLiteral(resourceName: "default_img")
        
        scroll.auk.startAutoScroll(delaySeconds: 3)
        scroll.auk.settings.contentMode = .scaleAspectFit
        scroll.auk.settings.pageControl.marginToScrollViewBottom = 50
        scroll.auk.settings.pageControl.currentPageIndicatorTintColor = UIColor.red
    }

    
    
    // MARK: - UIScrollViewDelegate

    func scrollViewDidScroll(_: UIScrollView)
    {
        
    }

    @IBAction func onScrollViewTapped(_: AnyObject)
    {

    }
}
