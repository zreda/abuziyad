	//
//  SideMenuCell.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 12/15/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    
    @IBOutlet weak var labelField: UILabel!
    @IBOutlet weak var imageField: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(_ data: [String: Any]) {
        
        labelField.text = data["name"] as? String
        if let img = data["image"] as? UIImage {
            imageField.image = img
        } else {
            imageField.image = nil
        }
        
    }

}
