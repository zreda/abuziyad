//
//  ContactUsViewController.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 11/11/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {

    @IBOutlet var inputFields: [DesignableUITextField]!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var descriptionContentTextView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    
    // MARK: - Methods
    
    // MARK: - Init
    
    /**
     Initialize an instance of Contact US View Controller
     - returns: ContactUsViewController Object
     */
    public static func create() -> ContactUsViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! ContactUsViewController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboard()
        title = "title_text".localized()
        descriptionTextView.text = "contact_us_text".localized()
    }

 
    @IBAction func sendFeedback() {
        
        if !validateData() { return }
        
        let jsonModel = ["email": inputFields[1].text!, "fullname": inputFields[0].text!, "msg": descriptionContentTextView.text!]
        
        let contactUsManager = AZContactUsRequest()
        contactUsManager.contactUsWithFeedback(feedback: jsonModel)
        contactUsManager.didSendFeedback = { (msg, error) in
            if let error = error {
                Helper.showFloatAlert(title: error.localizedDescription, subTitle:"" , type: Constants.AlertType.AlertError)
            } else {
                Helper.showFloatAlert(title: msg!, subTitle: "", type: Constants.AlertType.AlertError)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    private func validateData() -> Bool {
        
        for field in inputFields {
            if field.text?.isEmpty == true {
                Helper.showFloatAlert(title: "empty_field_msg".localized() , subTitle: "", type: Constants.AlertType.AlertError)
                return false
            }
        }
        
        if descriptionContentTextView.text?.isEmpty == true {
            Helper.showFloatAlert(title: "empty_field_msg".localized(), subTitle: "", type: Constants.AlertType.AlertError)
            return false
        }
        
        
        if(!Helper.isValidEmail(mail_address: inputFields[1].text!)){
            
            Helper.showFloatAlert(title: "invalid_mail_msg".localized(), subTitle: "", type: Constants.AlertType.AlertError)
            return false
        }
        return true
    }
}
