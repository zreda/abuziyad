//
//  OrderDetailsTableViewController.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 12/10/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class OrderDetailsTableViewController: UITableViewController {

    @IBOutlet weak var carType: UILabel!
    @IBOutlet weak var numberOfDays: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var telephone: UILabel!
    
    @IBOutlet weak var idNumber: UILabel!
    
    @IBOutlet weak var deliveryTime: UILabel!
    @IBOutlet weak var deliveryDate: UILabel!
    @IBOutlet weak var cityBranch: UILabel!
    @IBOutlet weak var licenceNumber: UILabel!
    var orderItem:OrderModelResponse?
    override func viewDidLoad() {
        super.viewDidLoad()
        carType.text  = orderItem?.carName ?? "---"
        numberOfDays.text = orderItem?.numberOfDays ?? "---"
        total.text = orderItem?.total ?? "---"
        fullName.text = orderItem?.fullName ?? "---"
        email.text = orderItem?.email ?? "---"
        telephone.text = orderItem?.mobileNo ?? "---"
        idNumber.text = orderItem?.idNo ?? "---"
        deliveryTime.text = orderItem?.receivedTime ?? "---"
        deliveryDate.text = orderItem?.receivedDate ?? "---"
        licenceNumber.text = orderItem?.lisenceNo ?? "---"
        cityBranch.text = orderItem?.cityAndBranch ?? "---"

    }

    public static func create() -> OrderDetailsTableViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.authSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! OrderDetailsTableViewController
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   

}
