//
//  OrdersTableViewController.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 12/8/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class OrdersTableViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var noOrders: UILabel!
    @IBOutlet weak var orderTB: UITableView!
    var orderArray:[OrderModelResponse] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        getOrderHistory()
        title = "Order History".localized()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     Initialize an instance of Contact US View Controller
     - returns: ContactUsViewController Object
     */
    public static func create() -> OrdersTableViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.authSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! OrdersTableViewController
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = orderTB.dequeueReusableCell(withIdentifier: "oder_cell", for: indexPath) as! OrderTableViewCell
        cell.showBtn.tag = indexPath.row
        cell.showBtn.addTarget(self, action: #selector(OrdersTableViewController.showBtnTapped), for: .touchUpInside)
        cell.configureCell(order:orderArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140.0
    }
    
    func showBtnTapped(sender : UIButton!)
    {

        let detailsOrderVC = OrderDetailsTableViewController.create()
        detailsOrderVC.orderItem = orderArray[sender.tag]
        self.navigationController?.pushViewController(detailsOrderVC, animated: true)
    
    }
    
}

extension OrdersTableViewController {
    
    func getOrderHistory()
    {
        OderManager().getOrderHistory { [weak self](response, error) in
            
            if error == nil
            {
                
                if response?.count != 0
                {
                    self?.orderArray = response ?? []
                    self?.orderTB.reloadData()
                    //                    Helper.animateTable(table: (self?.orderTB) ?? UITableView())
                    self?.noOrders.isHidden = true
                }
                else
                {
                    self?.noOrders.isHidden = false
                    
                }
                
            }
            else
            {
                Helper.showFloatAlert(title: "Something went wrong!".localized(), subTitle: "", type: Constants.AlertType.AlertError)
                
            }
        }
        
    }
}
