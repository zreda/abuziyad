//
//  ConfirmPaymentTableViewController.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 12/16/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class ConfirmPaymentTableViewController: UITableViewController {

    @IBOutlet weak var orderNo: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var paymentMethod: UILabel!
    
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var telephone: UILabel!
    
    @IBOutlet weak var idNumber: UILabel!
    
    @IBOutlet weak var licenceNumber: UILabel!
    
    @IBOutlet weak var cityAndBranch: UILabel!
    
    @IBOutlet weak var deliveryDate: UILabel!
    
    @IBOutlet weak var deliveryTime: UILabel!
    
    @IBOutlet weak var note: UILabel!
    var model:PuchaseModelResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        orderNo.text = String(describing:model?.orderNo as! Int)
        date.text = model?.date
        total.text = model?.total
        paymentMethod.text = model?.paymentMethod
        fullName.text = model?.fullName
        email.text = model?.email
        telephone.text = model?.mobileNo
        idNumber.text = model?.idNo
        licenceNumber.text = model?.lisenceNo
        cityAndBranch.text = model?.cityAndBranch
        deliveryDate.text = model?.receivedDate
        deliveryTime.text = model?.receivedTime
        note.text = model?.note

        
    }

    /**
     Initialize an instance of Contact US View Controller
     - returns: ContactUsViewController Object
     */
    public static func create() -> ConfirmPaymentTableViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.fleetSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! ConfirmPaymentTableViewController
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func okBtnAction(_ sender: Any) {
        self.navigationController?.pushViewController(OrdersTableViewController.create(), animated: true)
//        self.navigationController?.popViewController(animated: true)
    }
    
   
}
