//
//  PaymentTableViewController.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 12/9/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import RadioButton
import IQDropDownTextField
import AAPopUp

class PaymentTableViewController: UITableViewController  ,  IQDropDownTextFieldDelegate, UITextFieldDelegate{
    
    @IBOutlet weak var idNumber: DesignableUITextField!
    @IBOutlet weak var receivedDate: UITextField!
    @IBOutlet weak var receviedTime: UITextField!
    
    @IBOutlet weak var payCash: RadioButton!
    @IBOutlet weak var payVisa: RadioButton!
    
    @IBOutlet weak var masterCardNumber: UITextField!
    @IBOutlet weak var month: IQDropDownTextField!
    @IBOutlet weak var year: IQDropDownTextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var approveTerms: CheckBox!
    
//    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var creditCardView: UIView!
    
    
    @IBOutlet weak var agreeBtn: CheckBox!
    @IBOutlet weak var creditViewCons: NSLayoutConstraint!
    fileprivate var branches: [AZBranchesModel] = []
    @IBOutlet var inputFilters: [IQDropDownTextField]!

    
    var branchsItems:[AZBranch] = []
    var brachItemStr:[String] = []
    
    @IBOutlet weak var agreeLB: UILabel!
    
    
    var selectedCity = ""
    var selectedBranch = ""
    var carId:String?
    var userID:String?
    var numOfDays:String?
    
    
    let monthsArray:[String] = {
        
        var urlStr = ""
        
        if LanguageManger.shared.currentLang == "ar"
        {
            urlStr =  "Gregorian Month_ar"
        }
        else
        {
            urlStr =  "Gregorian Month_en"
        }
        if let url = Bundle.main.url(forResource:urlStr, withExtension: "plist") {
            if let strArr = NSMutableArray(contentsOf:url) as? [String] {
                return strArr
            }
            return []
        }
        return []
    }()
    
    let yearsArray:[String] = {
        
        if let url = Bundle.main.url(forResource:"Years_Expiration", withExtension: "plist") {
            if let strArr = NSMutableArray(contentsOf:url) as? [String] {
                return strArr
            }
            return []
        }
        return []
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        password.delegate = self
        masterCardNumber.text = "4054331260001462"
        creditCardView.isHidden = true
        let starttimedatePicker = UIDatePicker()
        starttimedatePicker.datePickerMode = .time
        
        
        let datepicker = UIDatePicker()
        datepicker.datePickerMode = .date
        datepicker.minimumDate = Date().tomorrow
        
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(PaymentTableViewController.agreeLbTapped))
        agreeLB.isUserInteractionEnabled = true
        agreeLB.addGestureRecognizer(tap)
        
        self.idNumber.text = ((Helper.getObjectDefault(key: Constants.userDefault.userData) as! UserDataModel).userLogin )
        
        
        receviedTime.inputView = starttimedatePicker
        starttimedatePicker.locale = NSLocale(localeIdentifier: "en_US") as Locale // using Great Britain for this example
        
        starttimedatePicker.addTarget(self, action: #selector(showTime), for: .valueChanged)
        

        datepicker.addTarget(self, action: #selector(dateChanged), for: .valueChanged)
        receivedDate.inputView = datepicker
        
    
        let branchesRequest = AZBranchesRequest()
        branchesRequest.getBranchesList()
        title = "title_text".localized()
        inputFilters[0].delegate = self
        inputFilters[1].delegate = self
        
        branchesRequest.didGetBranchesList = { [weak self] (branches, error) -> Void in
            if let error = error {
                Helper.showFloatAlert(title: error.localizedDescription, subTitle: "", type: Constants.AlertType.AlertError)
            } else {
                if let branches = branches {
                    var branchesList:[String] = ["All Branches"]
                    var citiesList: [String] = ["All Cities"]
                    
                    for b in branches {
                        citiesList.append(b.city)
                        for c in b.branch {
                            branchesList.append(c.name)
                            self?.branchsItems.append(c)
                            
                        }
                    }
                    self?.inputFilters[0].itemList = citiesList
                    //                    branchesList.insert("All Branches", at: 0)
                    self?.inputFilters[1].itemList = branchesList
                    self?.brachItemStr = branchesList
                    self?.branches = branches
                    self?.selectedCity = branches[0].city
                   
                }
            }
        }
        
        let date = Date().tomorrow
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd/MM/yyyy"
        receivedDate.text = formatter.string(from: date)
    
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .medium
        let timeString = "\(dateFormatter.string(from: Date() as Date))"
        receviedTime.text = String(timeString)
        
        year.itemList = yearsArray
        month.itemList = monthsArray
        
        self.hideKeyboard()
    }
    
    
    func agreeLbTapped(sender:UITapGestureRecognizer) {
        self.navigationController?.pushViewController(TermsAndConditionsViewController.create(), animated: true)
    }
    
    /**
     Initialize an instance of Contact US View Controller
     - returns: ContactUsViewController Object
     */
    public static func create() -> PaymentTableViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.fleetSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! PaymentTableViewController
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 3 // Bool
    }
    func dateChanged(sender: UIDatePicker) {
        
        let componenets = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        if let day = componenets.day, let month = componenets.month, let year = componenets.year {
            print("\(day) \(month) \(year)")
            
            receivedDate.text = "\(day)/\(month)/\(year)"
        }
    }
    
    
    func textField(_ textField: IQDropDownTextField, didSelectItem item: String?) {
        
        var branchToShow:[AZBranch] = []
        if selectedCity != item && textField == inputFilters[0]
        {
            
            self.branchsItems = self.branches[inputFilters[0].selectedRow-1 == -1 ? 0 : inputFilters[0].selectedRow-1].branch
            branchToShow = self.branchsItems
            selectedCity = item!
            self.brachItemStr = []
            for (_,item) in branchsItems.enumerated()
            {
                brachItemStr.append(item.name)
            }
            
            inputFilters[1].itemList = brachItemStr
        }
        
        if  selectedBranch != item && textField == inputFilters[1]
        {
            selectedBranch = item!
            branchToShow  = [self.branchsItems[inputFilters[1].selectedRow-1 == -1 ? 0 : inputFilters[0].selectedRow-1]]
            
        }
        
    }

    
    
    func showTime(sender: UIDatePicker)
    {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        receviedTime.text = formatter.string(from: sender.date)
        
    }
    
    
    @IBAction func confrimBtnTapped(_ sender: Any)
    {
        if inputFilters?[0].selectedItem != nil
        {
        selectedCity = (inputFilters?[0].selectedItem ?? "")! == "All Cities" ? "" : inputFilters[0].selectedItem!
        }
        if inputFilters?[1].selectedItem != nil
        {
        selectedBranch = (inputFilters?[1].selectedItem ?? "")! == "All Branches" ? "" : inputFilters[1].selectedItem!
        }
        if selectedCity == ""
        {
            Helper.showFloatAlert(title: "cityEmpty".localized(), subTitle: "", type: Constants.AlertType.AlertError)
            
        }
        else if selectedBranch == ""
        {
            Helper.showFloatAlert(title: "branchEmpty".localized(), subTitle: "", type: Constants.AlertType.AlertError)
            
        }
        else if receivedDate.text == ""
        {
            Helper.showFloatAlert(title: "receivedDateEmpty".localized(), subTitle: "", type: Constants.AlertType.AlertError)
            
        }
        else if receviedTime.text == ""
        {
            Helper.showFloatAlert(title: "receivedTimeEmpty".localized(), subTitle: "", type: Constants.AlertType.AlertError)
            
        }
        else if !agreeBtn.isChecked
        {
            Helper.showFloatAlert(title: "accept_terms".localized(), subTitle: "", type: Constants.AlertType.AlertError)
            
        }
        else
        {
            if !creditCardView.isHidden
            {
                if masterCardNumber.text == ""
                {
                    Helper.showFloatAlert(title: "masterCardEmpty".localized(), subTitle: "", type: Constants.AlertType.AlertError)
                    
                }
                else if month.selectedRow == -1
                {
                    Helper.showFloatAlert(title: "monthCardEmpty".localized(), subTitle: "", type: Constants.AlertType.AlertError)
                    
                }
                else if year.selectedRow == -1
                {
                    Helper.showFloatAlert(title: "yearCardEmpty".localized(), subTitle: "", type: Constants.AlertType.AlertError)
                    
                }
                else if password.text == ""
                {
                    Helper.showFloatAlert(title: "securityNumberEmpty".localized(), subTitle: "", type: Constants.AlertType.AlertError)
                    
                }
                else
                {
                    
                    
                    let requestParam = OrderByCreditModelRequest(branch: selectedBranch, carId: carId!, cardCsc: password.text!, cardExpMonth: String(format: "%02d", (month.selectedRow+1)), cardNumber: masterCardNumber.text!, city: selectedCity, expirationYear: String(year.selectedRow+17), numberOfDays: numOfDays!, receivedDate: receivedDate.text!, receivedTime: receviedTime.text!, userId: userID!)
                    
                    OderManager().OrderByCredit(orderRequest: requestParam, completion: { (response, error) in
                        
                        if error == nil
                        {
                            
                            if response?.msg != nil
                            {
                                Helper.showAlert(type: Constants.AlertType.AlertError, title: (response?.msg!)!, subTitle: "", closeTitle: "ok".localized())
                                
                            }
                            else
                            {
                                Helper.showAlert(type: Constants.AlertType.AlertSuccess, title: "Your order is added successfully".localized(), subTitle: "", closeTitle: "ok".localized())
                                let confirmVC = ConfirmPaymentTableViewController.create()
                                confirmVC.model = response
                                self.navigationController?.pushViewController(confirmVC, animated: true)
                            }
                        }
                        else
                        {
                            Helper.showAlert(type: Constants.AlertType.AlertSuccess, title: "Something went wrong!".localized(), subTitle: "", closeTitle: "ok".localized())
                            
                            
                        }
                    })
                }
            }
            else
            {
                let requestParam = OrderByCreditModelRequest(branch: selectedBranch, carId: carId!, city: selectedCity, numberOfDays: numOfDays!, receivedDate: receivedDate.text!, receivedTime: receviedTime.text!, userId: userID!)
                
                OderManager().OrderCash(orderRequest: requestParam, completion: { (response, error) in
                    
                    if error == nil
                    {
                        if response?.msg != nil
                        {
                            Helper.showAlert(type: Constants.AlertType.AlertError, title: "Your order is under process".localized(), subTitle: "", closeTitle: "ok".localized())
//                            self.navigationController?.popViewController(animated: true)
                            
                        }
                        else
                        {
                            Helper.showAlert(type: Constants.AlertType.AlertSuccess, title: "Your order is added successfully".localized(), subTitle: "", closeTitle: "ok".localized())
                            let confirmVC = ConfirmPaymentTableViewController.create()
                            confirmVC.model = response
                            self.navigationController?.pushViewController(confirmVC, animated: true)
                        }
                    }
                    else
                    {
                        Helper.showAlert(type: Constants.AlertType.AlertSuccess, title: "Something went wrong!".localized(), subTitle: "", closeTitle: "ok".localized())
                        
                    }
                })
            }
            
        }
    }
    
    @IBAction func payVisaBtnTapped(_ sender: Any) {
        
        
        payCash.setImage(#imageLiteral(resourceName: "unchecked_r") , for: UIControlState.normal)
        payVisa.setImage(#imageLiteral(resourceName: "checked_r") ,for: UIControlState.normal)
        creditCardView.isHidden = false
        self.tableView.reloadData()
        
    }
    
    @IBAction func payCashBtnTapped(_ sender: Any) {
        
        
        payVisa.setImage(#imageLiteral(resourceName: "unchecked_r"), for: UIControlState.normal)
        payCash.setImage(#imageLiteral(resourceName: "checked_r"), for: UIControlState.normal)

        creditCardView.isHidden = true
        self.tableView.reloadData()
        
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 6 && creditCardView.isHidden == false
        {
            return 280
        }
        else if indexPath.row == 6 && creditCardView.isHidden == true
        {
            return 110
        }
        return 60
    }
    
    
}
