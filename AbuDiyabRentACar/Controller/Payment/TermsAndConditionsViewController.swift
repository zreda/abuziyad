//
//  TermsAndConditionsViewController.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 12/9/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: UIViewController {

    @IBOutlet weak var termsView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "title_text".localized()
        termsView.text = "terms".localized()
    }

    @IBAction func approveBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /**
     Initialize an instance of Contact US View Controller
     - returns: ContactUsViewController Object
     */
    public static func create() -> TermsAndConditionsViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.fleetSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! TermsAndConditionsViewController
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
