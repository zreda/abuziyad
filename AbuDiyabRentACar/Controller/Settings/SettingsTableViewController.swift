//
//  SettingsTableViewController.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 11/11/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    // MARK: - Methods
    
    // MARK: - Init
    
    /**
     Initialize an instance of Settings View Controller
     - returns: SettingsTableViewController Object
     */
    @IBOutlet weak var saveBarBtn: UIBarButtonItem!
    override func viewDidAppear(_ animated: Bool) {
        
        self.saveBarBtn.title = "Save".localized()
    }
    private var selectedLanguage: String = "en"

    public static func create() -> SettingsTableViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! SettingsTableViewController
    }
    
    @IBAction func save(_ sender: UIButton) {
   
        
        let selectedLanguage:Languages = self.selectedLanguage == "en" ? .en : .ar
        
        // change the language
        LanguageManger.shared.setLanguage(language: selectedLanguage)
        
        // return to root view contoller and reload it
//        UIApplication.topViewController!.dismiss(animated: true) {
            let delegate = UIApplication.shared.delegate as! AppDelegate
            let storyboard = UIStoryboard(name: Constants.StoryBoard.mainSB, bundle: nil)
            delegate.window?.rootViewController = storyboard.instantiateInitialViewController()
//        }
        
    
        
//        changeCurrentLanguage()
        
        
        
    }
    
    private func changeCurrentLanguage() {
        
        let alert = UIAlertController(title: "common.lang.alert.title".localized(), message: "common.lang.alert.message".localized(), preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "ok".localized(), style: UIAlertActionStyle.destructive, handler: { _ in
            UserDefaults.standard.set(self.selectedLanguage, forKey: self.selectedLanguageKey)
            UserDefaults.standard.set([self.selectedLanguage], forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
        }))
        
        alert.addAction(UIAlertAction(title: "cancel".localized(), style: UIAlertActionStyle.cancel, handler: { _ in
            
            if let langStr = Helper.getUserDefault(key: self.selectedLanguageKey) as? String {
            }
            
        }))
        
        // show the alert
        present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "title_text".localized()
    }
    
  
    let selectedLanguageKey: String = "CurrentLanguage"

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            selectedLanguage = "en"
        default:
            selectedLanguage = "ar"
        }
        
    }

 


}
