//
//  AccountViewController.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 12/8/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {

    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var odersView: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "title_text".localized()
        profileView.isHidden = false
        odersView.isHidden = true
        segmentControl.setTitle("ACCOUNT DETAILS".localized(), forSegmentAt: 0)
        segmentControl.setTitle("ORDERS".localized(), forSegmentAt: 1)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     Initialize an instance of Contact US View Controller
     - returns: ContactUsViewController Object
     */
    public static func create() -> AccountViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.authSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! AccountViewController
    }
    @IBAction func segmentValueChanged(_ sender: Any)
    {
        
        switch segmentControl.selectedSegmentIndex
        {
        case 0:
            profileView.isHidden = false
            odersView.isHidden = true
        case 1:
            profileView.isHidden = true
            odersView.isHidden = false
        default:
            break;
        }
    }
    
   
}
