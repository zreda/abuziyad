//
//  ProfileTableViewController.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 12/8/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import IQDropDownTextField

class ProfileTableViewController: UITableViewController , UITextFieldDelegate {
    
    @IBOutlet weak var idNumber: DesignableUITextField!
    @IBOutlet weak var firstName: DesignableUITextField!
 
    @IBOutlet weak var email: DesignableUITextField!
    
    @IBOutlet weak var nationality: IQDropDownTextField!
    
    @IBOutlet weak var mobileNumber: DesignableUITextField!
    
    @IBOutlet weak var licenseNumber: DesignableUITextField!
     var userData = UserDataModel()
    
    @IBOutlet weak var address: DesignableUITextField!
    
    var selectedNationalityIndex:Int = -1
    var nationalityList:[NationalityModelResponse] = []
    var nationalities:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        licenseNumber.delegate = self
        self.hideKeyboard()
        getNationalities()
        if Helper.isKeyPresentInUserDefaults(key: Constants.userDefault.userData)
        {
             userData = Helper.getObjectDefault(key: Constants.userDefault.userData) as! UserDataModel
            idNumber.text = userData.userLogin
            email.text = userData.userEmail
            firstName.text = userData.displayName
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     Initialize an instance of Contact US View Controller
     - returns: ContactUsViewController Object
     */
    public static func create() -> ProfileTableViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.authSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! ProfileTableViewController
    }
    
    
    
    @IBAction func updateBtnTapped(_ sender: Any)
    {
        let data = ProfileRequestModel(address: address.text ?? "", firstName: firstName.text ?? "", idNo: idNumber.text ?? "", lastName: "", licenseNo: licenseNumber.text ?? "", mobileNo: mobileNumber.text ?? "", ntly: "", phoneNo: mobileNumber.text ?? "", secondName: "", userEmail: email.text ?? "", userMachineId: String(describing:userData.iD as!Int))
        
        AuthenticationManager().updateProfile(profileData: data) { (response, error) in
            
            if error == nil
            {
                
                self.navigationController?.popViewController(animated: true)
                Helper.showFloatAlert(title: "Profile Updated Successfully".localized(), subTitle: "", type: Constants.AlertType.AlertSuccess)

                
            }
            else
            {
                Helper.showFloatAlert(title: "Something went wrong!".localized(), subTitle: "", type: Constants.AlertType.AlertError)
                
            }
            
        }
    }
    @IBAction func UpdateBtnPasswordTapped(_ sender: Any)
    {
        self.navigationController?.pushViewController(UpdatePasswordViewController.create(), animated: true)
        
    }
    
}

extension ProfileTableViewController
{
    
    
    func getNationalities()
    {
        AuthenticationManager().getAllNationality { (response, error) in
            
            if response != nil
            {
                self.nationalities = []
                self.nationalityList = response!
                
                for (_,nationality) in (self.nationalityList.enumerated())
                {
                    self.nationalities.append((nationality.ntnlty ?? "")!)
                }
                self.nationality.itemList = self.nationalities
                
            }
            else
            {
                Helper.showFloatAlert(title: "Something went wrong!".localized(), subTitle: "", type: Constants.AlertType.AlertError)
                
            }
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 10 // Bool
    }
}
