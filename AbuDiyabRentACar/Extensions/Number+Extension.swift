//
//  Number+Extension.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 12/20/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation

extension Int {
    
    func getMonthName(_ formatString: String) -> String {
        
        var dateComponents = DateComponents()
        dateComponents.month = self
        let date = Calendar.current.date(from: dateComponents)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatString
        
        if let date = date {
            return dateFormatter.string(from: date).uppercased()
        }
        return ""
    }
}
