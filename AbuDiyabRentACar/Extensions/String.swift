//
//  String.swift
//  OrangeFootballClub
//
//  Created by Alaa Taher on 4/23/17.
//  Copyright © 2017 Alaa Taher. All rights reserved.
//

import Foundation

extension String {

   
    public func localized() ->String {
    
        let lang = LanguageManger.shared.currentLang 
    
        
        let path = Bundle.main.path(forResource: lang , ofType: "lproj")
        let bundle = Bundle(path: path ?? "")
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
    
    func replace(target: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }
   

}

