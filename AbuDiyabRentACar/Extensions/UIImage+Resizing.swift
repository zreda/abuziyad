//
//  UIImage+Resizing.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 12/20/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

public extension UIImage {
    
    
    /**
     This method resizes a specific image with aspect ratio
     
     - parameter image: the image that we need to resize
     - parameter newWidth: the image new width that we want to use as aspect ration factor
     
     - returns: The resized image
     
     */
     func resizeImage(newHeight: CGFloat) -> UIImage? {
        
        let scale = newHeight / self.size.height
        let newWidth = self.size.width * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
}
