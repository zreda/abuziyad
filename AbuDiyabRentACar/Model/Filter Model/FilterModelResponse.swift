//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class FilterModelResponse : NSObject, NSCoding, Mappable{

	var category : String?
	var id : Int?
	var machineName : String?


    override init() {
        super.init()
    }
    
    init(category : String , id : Int ,machineName : String) {
        self.category = category
        self.id = id
        self.machineName = machineName
    }
	class func newInstance(map: Map) -> Mappable?{
		return FilterModelResponse()
	}
	required init?(map: Map){}

	func mapping(map: Map)
	{
		category <- map["category"]
		id <- map["id"]
		machineName <- map["machine_name"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         category = aDecoder.decodeObject(forKey: "category") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         machineName = aDecoder.decodeObject(forKey: "machine_name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if category != nil{
			aCoder.encode(category, forKey: "category")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if machineName != nil{
			aCoder.encode(machineName, forKey: "machine_name")
		}

	}

}
