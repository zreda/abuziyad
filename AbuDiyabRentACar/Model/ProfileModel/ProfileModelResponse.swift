//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class ProfileModelResponse : NSObject, NSCoding, Mappable{

	var addressA : String?
	var firstName : String?
	var idNo : String?
	var lastName : String?
	var licenseNo : String?
	var mobileNumber : String?
	var ntnltyCode : String?
	var secondName : String?
	var userEmail : String?


	class func newInstance(map: Map) -> Mappable?{
		return ProfileModelResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		addressA <- map["address_a"]
		firstName <- map["first_name"]
		idNo <- map["id_no"]
		lastName <- map["last_name"]
		licenseNo <- map["license_no"]
		mobileNumber <- map["mobile_number"]
		ntnltyCode <- map["ntnlty_code"]
		secondName <- map["second_name"]
		userEmail <- map["user_email"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         addressA = aDecoder.decodeObject(forKey: "address_a") as? String
         firstName = aDecoder.decodeObject(forKey: "first_name") as? String
         idNo = aDecoder.decodeObject(forKey: "id_no") as? String
         lastName = aDecoder.decodeObject(forKey: "last_name") as? String
         licenseNo = aDecoder.decodeObject(forKey: "license_no") as? String
         mobileNumber = aDecoder.decodeObject(forKey: "mobile_number") as? String
         ntnltyCode = aDecoder.decodeObject(forKey: "ntnlty_code") as? String
         secondName = aDecoder.decodeObject(forKey: "second_name") as? String
         userEmail = aDecoder.decodeObject(forKey: "user_email") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if addressA != nil{
			aCoder.encode(addressA, forKey: "address_a")
		}
		if firstName != nil{
			aCoder.encode(firstName, forKey: "first_name")
		}
		if idNo != nil{
			aCoder.encode(idNo, forKey: "id_no")
		}
		if lastName != nil{
			aCoder.encode(lastName, forKey: "last_name")
		}
		if licenseNo != nil{
			aCoder.encode(licenseNo, forKey: "license_no")
		}
		if mobileNumber != nil{
			aCoder.encode(mobileNumber, forKey: "mobile_number")
		}
		if ntnltyCode != nil{
			aCoder.encode(ntnltyCode, forKey: "ntnlty_code")
		}
		if secondName != nil{
			aCoder.encode(secondName, forKey: "second_name")
		}
		if userEmail != nil{
			aCoder.encode(userEmail, forKey: "user_email")
		}

	}

}
