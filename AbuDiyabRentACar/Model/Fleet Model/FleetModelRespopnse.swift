//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class FleetModelRespopnse : NSObject, NSCoding, Mappable{

	var carCode : AnyObject?
	var carSlider : [String]?
	var carTransmission : String?
	var content : String?
	var cylinders : String?
	var doors : String?
	var driveType : String?
	var engineCapacity : String?
	var fuelType : String?
	var horsepower : String?
	var id : Int?
	var manufactureLogo : String?
	var manufactureName : String?
	var manufactureYear : String?
	var regularPrice : String?
	var remoteControl : String?
	var salePrice : String?
	var seats : String?
	var showcasePicture : AnyObject?
	var thumbnail : String?
	var title : String?
	var wheelsSize : String?


	class func newInstance(map: Map) -> Mappable?{
		return FleetModelRespopnse()
	}
	required init?(map: Map){}
	private override init(){}
    

     init(descriptionField : String,id : Int,image : String,regularPrice : String,salePrice : String,title : String) {
        self.id = id
        self.thumbnail = image
        self.regularPrice = regularPrice
        self.salePrice = salePrice
        self.title = title
        
    }

	func mapping(map: Map)
	{
		carCode <- map["car_code"]
		carSlider <- map["car_slider"]
		carTransmission <- map["car_transmission"]
		content <- map["content"]
		cylinders <- map["cylinders"]
		doors <- map["doors"]
		driveType <- map["drive_type"]
		engineCapacity <- map["engine_capacity"]
		fuelType <- map["fuel_type"]
		horsepower <- map["horsepower"]
		id <- map["id"]
		manufactureLogo <- map["manufacture_logo"]
		manufactureName <- map["manufacture_name"]
		manufactureYear <- map["manufacture_year"]
		regularPrice <- map["regular_price"]
		remoteControl <- map["remote_control"]
		salePrice <- map["sale_price"]
		seats <- map["seats"]
		showcasePicture <- map["showcase_picture"]
		thumbnail <- map["thumbnail"]
		title <- map["title"]
		wheelsSize <- map["wheels_size"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         carCode = aDecoder.decodeObject(forKey: "car_code") as? AnyObject
         carSlider = aDecoder.decodeObject(forKey: "car_slider") as? [String]
         carTransmission = aDecoder.decodeObject(forKey: "car_transmission") as? String
         content = aDecoder.decodeObject(forKey: "content") as? String
         cylinders = aDecoder.decodeObject(forKey: "cylinders") as? String
         doors = aDecoder.decodeObject(forKey: "doors") as? String
         driveType = aDecoder.decodeObject(forKey: "drive_type") as? String
         engineCapacity = aDecoder.decodeObject(forKey: "engine_capacity") as? String
         fuelType = aDecoder.decodeObject(forKey: "fuel_type") as? String
         horsepower = aDecoder.decodeObject(forKey: "horsepower") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         manufactureLogo = aDecoder.decodeObject(forKey: "manufacture_logo") as? String
         manufactureName = aDecoder.decodeObject(forKey: "manufacture_name") as? String
         manufactureYear = aDecoder.decodeObject(forKey: "manufacture_year") as? String
         regularPrice = aDecoder.decodeObject(forKey: "regular_price") as? String
         remoteControl = aDecoder.decodeObject(forKey: "remote_control") as? String
         salePrice = aDecoder.decodeObject(forKey: "sale_price") as? String
         seats = aDecoder.decodeObject(forKey: "seats") as? String
         showcasePicture = aDecoder.decodeObject(forKey: "showcase_picture") as? AnyObject
         thumbnail = aDecoder.decodeObject(forKey: "thumbnail") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
         wheelsSize = aDecoder.decodeObject(forKey: "wheels_size") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if carCode != nil{
			aCoder.encode(carCode, forKey: "car_code")
		}
		if carSlider != nil{
			aCoder.encode(carSlider, forKey: "car_slider")
		}
		if carTransmission != nil{
			aCoder.encode(carTransmission, forKey: "car_transmission")
		}
		if content != nil{
			aCoder.encode(content, forKey: "content")
		}
		if cylinders != nil{
			aCoder.encode(cylinders, forKey: "cylinders")
		}
		if doors != nil{
			aCoder.encode(doors, forKey: "doors")
		}
		if driveType != nil{
			aCoder.encode(driveType, forKey: "drive_type")
		}
		if engineCapacity != nil{
			aCoder.encode(engineCapacity, forKey: "engine_capacity")
		}
		if fuelType != nil{
			aCoder.encode(fuelType, forKey: "fuel_type")
		}
		if horsepower != nil{
			aCoder.encode(horsepower, forKey: "horsepower")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if manufactureLogo != nil{
			aCoder.encode(manufactureLogo, forKey: "manufacture_logo")
		}
		if manufactureName != nil{
			aCoder.encode(manufactureName, forKey: "manufacture_name")
		}
		if manufactureYear != nil{
			aCoder.encode(manufactureYear, forKey: "manufacture_year")
		}
		if regularPrice != nil{
			aCoder.encode(regularPrice, forKey: "regular_price")
		}
		if remoteControl != nil{
			aCoder.encode(remoteControl, forKey: "remote_control")
		}
		if salePrice != nil{
			aCoder.encode(salePrice, forKey: "sale_price")
		}
		if seats != nil{
			aCoder.encode(seats, forKey: "seats")
		}
		if showcasePicture != nil{
			aCoder.encode(showcasePicture, forKey: "showcase_picture")
		}
		if thumbnail != nil{
			aCoder.encode(thumbnail, forKey: "thumbnail")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if wheelsSize != nil{
			aCoder.encode(wheelsSize, forKey: "wheels_size")
		}

	}

}
