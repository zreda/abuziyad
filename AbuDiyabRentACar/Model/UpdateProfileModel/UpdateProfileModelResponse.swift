//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class UpdateProfileModelResponse : NSObject, NSCoding, Mappable{

	var msg : String?
    var error:String?


	class func newInstance(map: Map) -> Mappable?{
		return UpdateProfileModelResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		msg <- map["Msg"]
        error <- map["Error"]

	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         msg = aDecoder.decodeObject(forKey: "Msg") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if msg != nil{
			aCoder.encode(msg, forKey: "Msg")
		}

	}

}
