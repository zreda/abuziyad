//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class ProfileRequestModel : NSObject, NSCoding, Mappable{

	var address : String?
	var firstName : String?
	var idNo : String?
	var lastName : String?
	var licenseNo : String?
	var mobileNo : String?
	var ntly : String?
	var phoneNo : String?
	var secondName : String?
	var userEmail : String?
	var userMachineId : String?


	class func newInstance(map: Map) -> Mappable?{
		return ProfileRequestModel()
	}
  
    init( address : String , firstName : String ,idNo : String ,lastName : String , licenseNo : String ,mobileNo : String, ntly : String,phoneNo : String,secondName : String,userEmail : String,userMachineId : String) {
        self.address = address
        self.firstName = firstName
        self.idNo = idNo
        self.lastName = lastName
        self.licenseNo = licenseNo
        self.mobileNo = mobileNo
        self.ntly = ntly
        self.phoneNo = phoneNo
        self.userEmail = userEmail
        self.secondName = secondName
        self.userMachineId = userMachineId
    }
	required init?(map: Map){}
    
    override init(){
        super.init()
    }

	func mapping(map: Map)
	{
		address <- map["address"]
		firstName <- map["first_name"]
		idNo <- map["id_no"]
		lastName <- map["last_name"]
		licenseNo <- map["license_no"]
		mobileNo <- map["mobile_no"]
		ntly <- map["ntly"]
		phoneNo <- map["phone_no"]
		secondName <- map["second_name"]
		userEmail <- map["user_email"]
		userMachineId <- map["user_machine_id"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         firstName = aDecoder.decodeObject(forKey: "first_name") as? String
         idNo = aDecoder.decodeObject(forKey: "id_no") as? String
         lastName = aDecoder.decodeObject(forKey: "last_name") as? String
         licenseNo = aDecoder.decodeObject(forKey: "license_no") as? String
         mobileNo = aDecoder.decodeObject(forKey: "mobile_no") as? String
         ntly = aDecoder.decodeObject(forKey: "ntly") as? String
         phoneNo = aDecoder.decodeObject(forKey: "phone_no") as? String
         secondName = aDecoder.decodeObject(forKey: "second_name") as? String
         userEmail = aDecoder.decodeObject(forKey: "user_email") as? String
         userMachineId = aDecoder.decodeObject(forKey: "user_machine_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if firstName != nil{
			aCoder.encode(firstName, forKey: "first_name")
		}
		if idNo != nil{
			aCoder.encode(idNo, forKey: "id_no")
		}
		if lastName != nil{
			aCoder.encode(lastName, forKey: "last_name")
		}
		if licenseNo != nil{
			aCoder.encode(licenseNo, forKey: "license_no")
		}
		if mobileNo != nil{
			aCoder.encode(mobileNo, forKey: "mobile_no")
		}
		if ntly != nil{
			aCoder.encode(ntly, forKey: "ntly")
		}
		if phoneNo != nil{
			aCoder.encode(phoneNo, forKey: "phone_no")
		}
		if secondName != nil{
			aCoder.encode(secondName, forKey: "second_name")
		}
		if userEmail != nil{
			aCoder.encode(userEmail, forKey: "user_email")
		}
		if userMachineId != nil{
			aCoder.encode(userMachineId, forKey: "user_machine_id")
		}

	}

}
