//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class NationalityModelResponse : NSObject, NSCoding, Mappable{

	var ntnlty : String?
	var ntnltyCode : String?

	class func newInstance(map: Map) -> Mappable?{
		return NationalityModelResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		ntnlty <- map["ntnlty"]
		ntnltyCode <- map["ntnlty_code"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         ntnlty = aDecoder.decodeObject(forKey: "ntnlty") as? String
         ntnltyCode = aDecoder.decodeObject(forKey: "ntnlty_code") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if ntnlty != nil{
			aCoder.encode(ntnlty, forKey: "ntnlty")
		}
		if ntnltyCode != nil{
			aCoder.encode(ntnltyCode, forKey: "ntnlty_code")
		}

	}

}
