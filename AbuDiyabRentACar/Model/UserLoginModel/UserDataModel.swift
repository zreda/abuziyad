//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class UserDataModel : NSObject, NSCoding, Mappable{

	var iD : Int?
	var displayName : String?
	var userActivationKey : String?
	var userEmail : String?
	var userLogin : String?
	var userNicename : String?
	var userPass : String?
	var userRegistered : String?
	var userStatus : String?
	var userUrl : String?


	class func newInstance(map: Map) -> Mappable?{
		return UserDataModel()
	}
    override init() {
        
    }
	required init?(map: Map){}

	func mapping(map: Map)
	{
		iD <- map["ID"]
		displayName <- map["display_name"]
		userActivationKey <- map["user_activation_key"]
		userEmail <- map["user_email"]
		userLogin <- map["user_login"]
		userNicename <- map["user_nicename"]
		userPass <- map["user_pass"]
		userRegistered <- map["user_registered"]
		userStatus <- map["user_status"]
		userUrl <- map["user_url"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         iD = aDecoder.decodeObject(forKey: "ID") as? Int
         displayName = aDecoder.decodeObject(forKey: "display_name") as? String
         userActivationKey = aDecoder.decodeObject(forKey: "user_activation_key") as? String
         userEmail = aDecoder.decodeObject(forKey: "user_email") as? String
         userLogin = aDecoder.decodeObject(forKey: "user_login") as? String
         userNicename = aDecoder.decodeObject(forKey: "user_nicename") as? String
         userPass = aDecoder.decodeObject(forKey: "user_pass") as? String
         userRegistered = aDecoder.decodeObject(forKey: "user_registered") as? String
         userStatus = aDecoder.decodeObject(forKey: "user_status") as? String
         userUrl = aDecoder.decodeObject(forKey: "user_url") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if iD != nil{
			aCoder.encode(iD, forKey: "ID")
		}
		if displayName != nil{
			aCoder.encode(displayName, forKey: "display_name")
		}
		if userActivationKey != nil{
			aCoder.encode(userActivationKey, forKey: "user_activation_key")
		}
		if userEmail != nil{
			aCoder.encode(userEmail, forKey: "user_email")
		}
		if userLogin != nil{
			aCoder.encode(userLogin, forKey: "user_login")
		}
		if userNicename != nil{
			aCoder.encode(userNicename, forKey: "user_nicename")
		}
		if userPass != nil{
			aCoder.encode(userPass, forKey: "user_pass")
		}
		if userRegistered != nil{
			aCoder.encode(userRegistered, forKey: "user_registered")
		}
		if userStatus != nil{
			aCoder.encode(userStatus, forKey: "user_status")
		}
		if userUrl != nil{
			aCoder.encode(userUrl, forKey: "user_url")
		}

	}

}
