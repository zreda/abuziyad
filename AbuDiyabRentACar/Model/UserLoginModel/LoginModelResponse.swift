//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class LoginModelResponse : NSObject, NSCoding, Mappable{

	var iD : Int?
	var data : UserDataModel?
    var error : Int?
    var msg : String?


	class func newInstance(map: Map) -> Mappable?{
		return LoginModelResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		iD    <- map["ID"]
		data  <- map["data"]
        error <- map["Error"]
        msg    <- map["Msg"]

		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         iD = aDecoder.decodeObject(forKey: "ID") as? Int
         data = aDecoder.decodeObject(forKey: "data") as? UserDataModel

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if iD != nil{
			aCoder.encode(iD, forKey: "ID")
		}
		if data != nil{
			aCoder.encode(data, forKey: "data")
		}

	}

}
