//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OrderByCreditModelRequest : NSObject, NSCoding{

	var branch : String!
	var carId : String!
	var cardCsc : String!
	var cardExpMonth : String!
	var cardNumber : String!
	var city : String!
	var expirationYear : String!
	var numberOfDays : String!
	var receivedDate : String!
	var receivedTime : String!
	var userId : String!

    override init() {
        
    }
    init( branch : String, carId : String,cardCsc : String, cardExpMonth : String, cardNumber : String,city : String,expirationYear : String,numberOfDays : String,receivedDate : String,receivedTime : String, userId : String) {
        self.branch = branch
        self.carId = carId
        self.cardCsc = cardCsc
        self.cardExpMonth = cardExpMonth
        self.city = city
        self.cardNumber = cardNumber
        self.expirationYear = expirationYear
        self.numberOfDays = numberOfDays
        self.receivedDate = receivedDate
        self.receivedTime = receivedTime
        self.userId = userId
    }

    init( branch : String, carId : String, city : String,numberOfDays : String,receivedDate : String,receivedTime : String, userId : String) {
        self.branch = branch
        self.carId = carId
        self.cardCsc = ""
        self.cardExpMonth = ""
        self.city = city
        self.cardNumber = ""
        self.expirationYear = ""
        self.numberOfDays = numberOfDays
        self.receivedDate = receivedDate
        self.receivedTime = receivedTime
        self.userId = userId
    }

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		branch = dictionary["branch"] as? String
		carId = dictionary["car_id"] as? String
		cardCsc = dictionary["card_csc"] as? String
		cardExpMonth = dictionary["card_exp_month"] as? String
		cardNumber = dictionary["card_number"] as? String
		city = dictionary["city"] as? String
		expirationYear = dictionary["expirationYear"] as? String
		numberOfDays = dictionary["number_of_days"] as? String
		receivedDate = dictionary["received_date"] as? String
		receivedTime = dictionary["received_time"] as? String
		userId = dictionary["user_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if branch != nil{
			dictionary["branch"] = branch
		}
		if carId != nil{
			dictionary["car_id"] = carId
		}
		if cardCsc != nil{
			dictionary["card_csc"] = cardCsc
		}
		if cardExpMonth != nil{
			dictionary["card_exp_month"] = cardExpMonth
		}
		if cardNumber != nil{
			dictionary["card_number"] = cardNumber
		}
		if city != nil{
			dictionary["city"] = city
		}
		if expirationYear != nil{
			dictionary["expirationYear"] = expirationYear
		}
		if numberOfDays != nil{
			dictionary["number_of_days"] = numberOfDays
		}
		if receivedDate != nil{
			dictionary["received_date"] = receivedDate
		}
		if receivedTime != nil{
			dictionary["received_time"] = receivedTime
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         branch = aDecoder.decodeObject(forKey: "branch") as? String
         carId = aDecoder.decodeObject(forKey: "car_id") as? String
         cardCsc = aDecoder.decodeObject(forKey: "card_csc") as? String
         cardExpMonth = aDecoder.decodeObject(forKey: "card_exp_month") as? String
         cardNumber = aDecoder.decodeObject(forKey: "card_number") as? String
         city = aDecoder.decodeObject(forKey: "city") as? String
         expirationYear = aDecoder.decodeObject(forKey: "expirationYear") as? String
         numberOfDays = aDecoder.decodeObject(forKey: "number_of_days") as? String
         receivedDate = aDecoder.decodeObject(forKey: "received_date") as? String
         receivedTime = aDecoder.decodeObject(forKey: "received_time") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if branch != nil{
			aCoder.encode(branch, forKey: "branch")
		}
		if carId != nil{
			aCoder.encode(carId, forKey: "car_id")
		}
		if cardCsc != nil{
			aCoder.encode(cardCsc, forKey: "card_csc")
		}
		if cardExpMonth != nil{
			aCoder.encode(cardExpMonth, forKey: "card_exp_month")
		}
		if cardNumber != nil{
			aCoder.encode(cardNumber, forKey: "card_number")
		}
		if city != nil{
			aCoder.encode(city, forKey: "city")
		}
		if expirationYear != nil{
			aCoder.encode(expirationYear, forKey: "expirationYear")
		}
		if numberOfDays != nil{
			aCoder.encode(numberOfDays, forKey: "number_of_days")
		}
		if receivedDate != nil{
			aCoder.encode(receivedDate, forKey: "received_date")
		}
		if receivedTime != nil{
			aCoder.encode(receivedTime, forKey: "received_time")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}

	}

}
