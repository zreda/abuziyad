//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class PuchaseModelResponse : NSObject, NSCoding, Mappable{

	var date : String?
	var numberOfDays : String?
	var total : String?
	var carName : String?
	var cityAndBranch : String?
	var email : String?
	var fullName : String?
	var idNo : String?
	var lisenceNo : String?
	var mobileNo : String?
	var note : String?
	var orderNo : Int?
	var paymentMethod : String?
	var receivedDate : String?
	var receivedTime : String?
    var error:String?
    var msg:String?


	class func newInstance(map: Map) -> Mappable?{
		return PuchaseModelResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		date <- map["Date"]
		numberOfDays <- map["Number_of_days"]
		total <- map["Total"]
		carName <- map["car_name"]
		cityAndBranch <- map["city_and_branch"]
		email <- map["email"]
		fullName <- map["full_name"]
		idNo <- map["id_no"]
		lisenceNo <- map["lisence_no"]
		mobileNo <- map["mobile_no"]
		note <- map["note"]
		orderNo <- map["order_no"]
		paymentMethod <- map["payment_method"]
		receivedDate <- map["received_date"]
		receivedTime <- map["received_time"]
        error <- map["Error"]
        msg <- map["Msg"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         date = aDecoder.decodeObject(forKey: "Date") as? String
         numberOfDays = aDecoder.decodeObject(forKey: "Number_of_days") as? String
         total = aDecoder.decodeObject(forKey: "Total") as? String
         carName = aDecoder.decodeObject(forKey: "car_name") as? String
         cityAndBranch = aDecoder.decodeObject(forKey: "city_and_branch") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         fullName = aDecoder.decodeObject(forKey: "full_name") as? String
         idNo = aDecoder.decodeObject(forKey: "id_no") as? String
         lisenceNo = aDecoder.decodeObject(forKey: "lisence_no") as? String
         mobileNo = aDecoder.decodeObject(forKey: "mobile_no") as? String
         note = aDecoder.decodeObject(forKey: "note") as? String
         orderNo = aDecoder.decodeObject(forKey: "order_no") as? Int
         paymentMethod = aDecoder.decodeObject(forKey: "payment_method") as? String
         receivedDate = aDecoder.decodeObject(forKey: "received_date") as? String
         receivedTime = aDecoder.decodeObject(forKey: "received_time") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if date != nil{
			aCoder.encode(date, forKey: "Date")
		}
		if numberOfDays != nil{
			aCoder.encode(numberOfDays, forKey: "Number_of_days")
		}
		if total != nil{
			aCoder.encode(total, forKey: "Total")
		}
		if carName != nil{
			aCoder.encode(carName, forKey: "car_name")
		}
		if cityAndBranch != nil{
			aCoder.encode(cityAndBranch, forKey: "city_and_branch")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if fullName != nil{
			aCoder.encode(fullName, forKey: "full_name")
		}
		if idNo != nil{
			aCoder.encode(idNo, forKey: "id_no")
		}
		if lisenceNo != nil{
			aCoder.encode(lisenceNo, forKey: "lisence_no")
		}
		if mobileNo != nil{
			aCoder.encode(mobileNo, forKey: "mobile_no")
		}
		if note != nil{
			aCoder.encode(note, forKey: "note")
		}
		if orderNo != nil{
			aCoder.encode(orderNo, forKey: "order_no")
		}
		if paymentMethod != nil{
			aCoder.encode(paymentMethod, forKey: "payment_method")
		}
		if receivedDate != nil{
			aCoder.encode(receivedDate, forKey: "received_date")
		}
		if receivedTime != nil{
			aCoder.encode(receivedTime, forKey: "received_time")
		}

	}

}
