//
//    RootClass.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import ObjectMapper


class OrderModelResponse : NSObject, NSCoding, Mappable{
    
    var numberOfDays : String?
    var carName : String?
    var cityAndBranch : String?
    var date : String?
    var email : String?
    var fullName : String?
    var idNo : String?
    var lisenceNo : String?
    var mobileNo : String?
    var note : String?
    var orderId : Int?
    var receivedDate : String?
    var receivedTime : String?
    var status : String?
    var total : String?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return OrderModelResponse()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        numberOfDays <- map["Number_of_days"]
        carName <- map["car_name"]
        cityAndBranch <- map["city_and_branch"]
        date <- map["date"]
        email <- map["email"]
        fullName <- map["full_name"]
        idNo <- map["id_no"]
        lisenceNo <- map["lisence_no"]
        mobileNo <- map["mobile_no"]
        note <- map["note"]
        orderId <- map["order_id"]
        receivedDate <- map["received_date"]
        receivedTime <- map["received_time"]
        status <- map["status"]
        total <- map["total"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        numberOfDays = aDecoder.decodeObject(forKey: "Number_of_days") as? String
        carName = aDecoder.decodeObject(forKey: "car_name") as? String
        cityAndBranch = aDecoder.decodeObject(forKey: "city_and_branch") as? String
        date = aDecoder.decodeObject(forKey: "date") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        fullName = aDecoder.decodeObject(forKey: "full_name") as? String
        idNo = aDecoder.decodeObject(forKey: "id_no") as? String
        lisenceNo = aDecoder.decodeObject(forKey: "lisence_no") as? String
        mobileNo = aDecoder.decodeObject(forKey: "mobile_no") as? String
        note = aDecoder.decodeObject(forKey: "note") as? String
        orderId = aDecoder.decodeObject(forKey: "order_id") as? Int
        receivedDate = aDecoder.decodeObject(forKey: "received_date") as? String
        receivedTime = aDecoder.decodeObject(forKey: "received_time") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        total = aDecoder.decodeObject(forKey: "total") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if numberOfDays != nil{
            aCoder.encode(numberOfDays, forKey: "Number_of_days")
        }
        if carName != nil{
            aCoder.encode(carName, forKey: "car_name")
        }
        if cityAndBranch != nil{
            aCoder.encode(cityAndBranch, forKey: "city_and_branch")
        }
        if date != nil{
            aCoder.encode(date, forKey: "date")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if fullName != nil{
            aCoder.encode(fullName, forKey: "full_name")
        }
        if idNo != nil{
            aCoder.encode(idNo, forKey: "id_no")
        }
        if lisenceNo != nil{
            aCoder.encode(lisenceNo, forKey: "lisence_no")
        }
        if mobileNo != nil{
            aCoder.encode(mobileNo, forKey: "mobile_no")
        }
        if note != nil{
            aCoder.encode(note, forKey: "note")
        }
        if orderId != nil{
            aCoder.encode(orderId, forKey: "order_id")
        }
        if receivedDate != nil{
            aCoder.encode(receivedDate, forKey: "received_date")
        }
        if receivedTime != nil{
            aCoder.encode(receivedTime, forKey: "received_time")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if total != nil{
            aCoder.encode(total, forKey: "total")
        }
        
    }
    
}

