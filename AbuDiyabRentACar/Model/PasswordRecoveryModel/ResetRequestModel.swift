//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class ResetRequestModel : NSObject, NSCoding, Mappable{

	var conPassword : String?
	var password : String?
	var userId : String?
	var username : String?

    init( conPassword : String ,password : String , userId : String ,username : String) {
        self.conPassword = conPassword
        self.password = password
        self.userId = userId
        self.username = username
    }

	class func newInstance(map: Map) -> Mappable?{
		return ResetRequestModel()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		conPassword <- map["con_password"]
		password <- map["password"]
		userId <- map["user_id"]
		username <- map["username"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         conPassword = aDecoder.decodeObject(forKey: "con_password") as? String
         password = aDecoder.decodeObject(forKey: "password") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? String
         username = aDecoder.decodeObject(forKey: "username") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if conPassword != nil{
			aCoder.encode(conPassword, forKey: "con_password")
		}
		if password != nil{
			aCoder.encode(password, forKey: "password")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}
		if username != nil{
			aCoder.encode(username, forKey: "username")
		}

	}

}
