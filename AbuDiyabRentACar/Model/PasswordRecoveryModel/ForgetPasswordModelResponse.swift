//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class ForgetPasswordModelResponse : NSObject, NSCoding, Mappable
{
    var msg : String?
    var returnField : Bool?
    var Error : Bool?

    
	class func newInstance(map: Map) -> Mappable?{
		return ForgetPasswordModelResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		msg <- map["msg"]
		returnField <- map["return"]
        Error <- map["Error"]

		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         msg = aDecoder.decodeObject(forKey: "msg") as? String
         returnField = aDecoder.decodeObject(forKey: "return") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if msg != nil{
			aCoder.encode(msg, forKey: "msg")
		}
		if returnField != nil{
			aCoder.encode(returnField, forKey: "return")
		}

	}

}
