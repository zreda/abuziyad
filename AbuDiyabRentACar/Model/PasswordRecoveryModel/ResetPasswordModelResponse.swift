//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class ResetPasswordModelResponse : NSObject, NSCoding, Mappable{

	var error : Bool?
	var msg : String?


	class func newInstance(map: Map) -> Mappable?{
		return ResetPasswordModelResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		error <- map["Error"]
		msg <- map["Msg"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         error = aDecoder.decodeObject(forKey: "Error") as? Bool
         msg = aDecoder.decodeObject(forKey: "Msg") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if error != nil{
			aCoder.encode(error, forKey: "Error")
		}
		if msg != nil{
			aCoder.encode(msg, forKey: "Msg")
		}

	}

}
