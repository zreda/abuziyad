//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class RegisterModelResponse : NSObject, NSCoding, Mappable{

	var id : Int?
	var msg : String?
	var reg : Bool?


	class func newInstance(map: Map) -> Mappable?{
		return RegisterModelResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		id <- map["id"]
		msg <- map["msg"]
		reg <- map["reg"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? Int
         msg = aDecoder.decodeObject(forKey: "msg") as? String
         reg = aDecoder.decodeObject(forKey: "reg") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if msg != nil{
			aCoder.encode(msg, forKey: "msg")
		}
		if reg != nil{
			aCoder.encode(reg, forKey: "reg")
		}

	}

}
