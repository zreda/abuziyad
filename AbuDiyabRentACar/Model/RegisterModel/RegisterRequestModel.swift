//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class RegisterRequestModel : NSObject, NSCoding{
	var addressA : String!
	var copyNo : String!
	var dayIdExpiryDt : String!
	var dayLcnsEndDate : String!
	var email : String!
	var firstName : String!
	var lastName : String!
	var licenseNo : String!
	var mobileNumber : String!
	var ntnltyCode : String!
	var password : String!
	var phoneNumber : String!
	var secondName : String!
	var username : String!


    override init() {
        super.init()
    }
    init(addressA : String,copyNo : String,dayIdExpiryDt : String,dayLcnsEndDate : String!
    , email : String, firstName : String,lastName : String, licenseNo : String, mobileNumber : String, ntnltyCode : String, password : String, phoneNumber : String, secondName : String,username : String) {
        
        self.addressA = addressA
        self.copyNo = copyNo
        self.dayIdExpiryDt = dayIdExpiryDt
        self.dayLcnsEndDate = dayLcnsEndDate
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        self.licenseNo = licenseNo
        self.mobileNumber = mobileNumber
        self.ntnltyCode = ntnltyCode
        self.password = password
        self.phoneNumber = phoneNumber
        self.secondName = secondName
        self.username = username
        
    }
	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		addressA = dictionary["address_a"] as? String
		copyNo = dictionary["copy_no"] as? String
		dayIdExpiryDt = dictionary["day_id_expiry_dt"] as? String
		dayLcnsEndDate = dictionary["day_lcns_end_date"] as? String
		email = dictionary["email"] as? String
		firstName = dictionary["first_name"] as? String
		lastName = dictionary["last_name"] as? String
		licenseNo = dictionary["license_no"] as? String
		mobileNumber = dictionary["mobile_number"] as? String
		ntnltyCode = dictionary["ntnlty_code"] as? String
		password = dictionary["password"] as? String
		phoneNumber = dictionary["phone_number"] as? String
		secondName = dictionary["second_name"] as? String
		username = dictionary["username"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if addressA != nil{
			dictionary["address_a"] = addressA
		}
		if copyNo != nil{
			dictionary["copy_no"] = copyNo
		}
		if dayIdExpiryDt != nil{
			dictionary["day_id_expiry_dt"] = dayIdExpiryDt
		}
		if dayLcnsEndDate != nil{
			dictionary["day_lcns_end_date"] = dayLcnsEndDate
		}
		if email != nil{
			dictionary["email"] = email
		}
		if firstName != nil{
			dictionary["first_name"] = firstName
		}
		if lastName != nil{
			dictionary["last_name"] = lastName
		}
		if licenseNo != nil{
			dictionary["license_no"] = licenseNo
		}
		if mobileNumber != nil{
			dictionary["mobile_number"] = mobileNumber
		}
		if ntnltyCode != nil{
			dictionary["ntnlty_code"] = ntnltyCode
		}
		if password != nil{
			dictionary["password"] = password
		}
		if phoneNumber != nil{
			dictionary["phone_number"] = phoneNumber
		}
		if secondName != nil{
			dictionary["second_name"] = secondName
		}
		if username != nil{
			dictionary["username"] = username
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         addressA = aDecoder.decodeObject(forKey: "address_a") as? String
         copyNo = aDecoder.decodeObject(forKey: "copy_no") as? String
         dayIdExpiryDt = aDecoder.decodeObject(forKey: "day_id_expiry_dt") as? String
         dayLcnsEndDate = aDecoder.decodeObject(forKey: "day_lcns_end_date") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         firstName = aDecoder.decodeObject(forKey: "first_name") as? String
         lastName = aDecoder.decodeObject(forKey: "last_name") as? String
         licenseNo = aDecoder.decodeObject(forKey: "license_no") as? String
         mobileNumber = aDecoder.decodeObject(forKey: "mobile_number") as? String
         ntnltyCode = aDecoder.decodeObject(forKey: "ntnlty_code") as? String
         password = aDecoder.decodeObject(forKey: "password") as? String
         phoneNumber = aDecoder.decodeObject(forKey: "phone_number") as? String
         secondName = aDecoder.decodeObject(forKey: "second_name") as? String
         username = aDecoder.decodeObject(forKey: "username") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if addressA != nil{
			aCoder.encode(addressA, forKey: "address_a")
		}
		if copyNo != nil{
			aCoder.encode(copyNo, forKey: "copy_no")
		}
		if dayIdExpiryDt != nil{
			aCoder.encode(dayIdExpiryDt, forKey: "day_id_expiry_dt")
		}
		if dayLcnsEndDate != nil{
			aCoder.encode(dayLcnsEndDate, forKey: "day_lcns_end_date")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if firstName != nil{
			aCoder.encode(firstName, forKey: "first_name")
		}
		if lastName != nil{
			aCoder.encode(lastName, forKey: "last_name")
		}
		if licenseNo != nil{
			aCoder.encode(licenseNo, forKey: "license_no")
		}
		if mobileNumber != nil{
			aCoder.encode(mobileNumber, forKey: "mobile_number")
		}
		if ntnltyCode != nil{
			aCoder.encode(ntnltyCode, forKey: "ntnlty_code")
		}
		if password != nil{
			aCoder.encode(password, forKey: "password")
		}
		if phoneNumber != nil{
			aCoder.encode(phoneNumber, forKey: "phone_number")
		}
		if secondName != nil{
			aCoder.encode(secondName, forKey: "second_name")
		}
		if username != nil{
			aCoder.encode(username, forKey: "username")
		}

	}

}
