//
//  UILabel+AZ.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 12/9/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import IQDropDownTextField

extension UILabel {
    
    /* Localized title for every label */
    @IBInspectable var localizedTitle: String {
        set {
            text = newValue.localized()
        }
        get {
            return text ?? "Not Localized"
        }
}
}


extension UIButton {
    
    /* Localized title for every label */
    @IBInspectable var localizedTitle: String {
        set {
            setTitle(newValue.localized(), for: .normal)
        }
        get {
            return titleLabel?.text ?? "Not Localized"
        }
    }
}

extension DesignableUITextField {
    
    /* Localized title for every label */
    @IBInspectable var localizedTitle: String {
        set {
            placeholder = newValue.localized()
        }
        get {
            return text ?? "Not Localized"
        }
    }
}

extension IQDropDownTextField {
    
    /* Localized title for every label */
    @IBInspectable var localizedTitle: String {
        set {
            placeholder = newValue.localized()
        }
        get {
            return placeholder ?? "Not Localized"
        }
    }
}

