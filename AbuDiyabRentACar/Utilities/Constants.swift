
import UIKit

let fleetCellHeight:CGFloat = 300.0
let fleetCellWidth:CGFloat = 300.0

class Constants: NSObject {
    
    let main_url:String = "http://80.48.143.107/"
    
    static let rulesTabs :[String] = ["CHANNELS".localized(),"SENDERS".localized(),"KEYWORDS".localized()]
    struct Url {
        
        static let login_url = Constants().main_url+"%@/verify"
        static let fetch_mail = Constants().main_url+"%@/from_app_list"
        static let fetch_mail_details = Constants().main_url+"%@/from_app_detail"
        static let user_channels = Constants().main_url+"/user-channel"
        static let user_feeds_channels = Constants().main_url+"/user-channel/%@/feeds"
    }
    
    
    struct Channels {
        
        static let twitterChannel = "Twitter"
        static let plazzaChannel = "Plazza"
        static let exchangeChannel = "Exchange"
    }
 
    
    struct StatusCode {
        
        static let StatusOK = 200
        static let StatusBadRequest = 400
        static let StatusNotfound = 404
        static let UserNotAuthorized = 401
        static let UserForbidden = 403
        static let Failure = 500
        static let Unavailable = 503
        
        

        
    }
    
    struct headers {
        static let accessKey = "access_key"
        static let accessKeyValue = "RcWr19Ws52Cv2c20ASh221Sa"
        static let accessPasswordKey = "access_password"
        static let accessPasswordValue = "LKJKm52AS2h1h1hQAW1a454qrlMll"
        static let applicationTypeKey = "Content-Type"
        static let applicationTypeValue = "application/json"
        
        
    }
    
    struct AlertType {
        static let AlertSuccess = 2
        static let AlertError = 1
        static let Alertinfo = 3
        static let AlertWarn = 4
    }
    
    struct userDefault {
        static let userData = "userData"
        static let isSkip = "isSkip"
    }
    
    struct StoryBoard {
        static let mainSB = "MainStoryboard"
        static let fleetSB = "FleetStoryboard"

        static let authSB = "AuthenticationStoryboard"
    }
    
    
}
