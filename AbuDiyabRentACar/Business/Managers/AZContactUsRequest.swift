//
//  AZContactUsRequest.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 11/30/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import Gloss


class AZContactUsRequest {
    
    var didSendFeedback: ((String?, Error?) -> Void)!
    
    func contactUsWithFeedback(feedback: [String: String]) {
        
        AppServiceManager.doRequest(path: .contactUs, method: .post, params: feedback, success: {
            [weak self] (responseObj) in
            
            guard let data = responseObj as? [String: String], let msg = data["Msg"] else {
                
                let err = NSError(domain: "somedomain", code: 123, userInfo: [NSLocalizedDescriptionKey : "generic_error_msg".localized()])
                self?.didSendFeedback(nil, err)
                return
            }
            self?.didSendFeedback(msg, nil)
            }, failure: { (error) in
                self.didSendFeedback(nil, error)
        })
    }
    
}
