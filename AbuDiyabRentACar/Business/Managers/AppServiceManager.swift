//
//  AppServiceManager.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 11/30/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import SVProgressHUD
import Alamofire
import Gloss

class AppServiceManager {


    class func doRequest(path: AZUrls.Path,
                         method: HTTPMethod,
                         params: [String: Any]? = nil,
                         success: @escaping (_ result: Any) -> Void,
                         failure: @escaping (_ err: Error) -> Void) {
        
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        SVProgressHUD.show()
        
        debugPrint("Get Request URL: \(path)")
        
        var urlString = "\(urlEnvironment.getBaseUrl())\(path.absolutePath)" 
        urlString += "&lang=\(LanguageManger.shared.currentLang)"
       
        let pathUrl = URL(string: urlString)!
        Alamofire.request(pathUrl,
                          method: method,
                          parameters: params,
                          encoding: JSONEncoding.default,
                          headers: nil).responseJSON { (response: DataResponse<Any>) in
                            
                            DispatchQueue.main.async(execute: {
                                SVProgressHUD.dismiss()
                            })
                            
                            switch response.result {
                            case .success(let value):
                                success(value)
                            case .failure(let error):
                                debugPrint(error)
                                failure(error)
                            }
        }
    }
    
    class var urlEnvironment: AZUrls.UrlEnvironment {
        
        return .baseUrl
    }
}
