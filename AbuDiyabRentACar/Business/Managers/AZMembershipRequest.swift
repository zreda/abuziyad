//
//  AZMembershipRequest.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 12/8/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import Gloss
import ObjectMapper


class AZMembershipRequest {
    
    var didGetMembershipRequest: (([AZMembershipModel]?, Error?) -> Void)!

    var didCheckOrder: ((String?, Error?) -> Void)!
    
    func getMembershipData() {
        
        AppServiceManager.doRequest(path: .membership, method: .get, success: { [weak self] (response) in
            
            guard let response = response as? [JSON], let membershipData = [AZMembershipModel].from(jsonArray: response) else {
                
                let err = NSError(domain: "somedomain", code: 123, userInfo: [NSLocalizedDescriptionKey : "generic_error_msg".localized()])
                self?.didGetMembershipRequest(nil, err)
                return
            }
            self?.didGetMembershipRequest(membershipData, nil)
            }, failure: { (error) in
                if let noItems = self.didGetMembershipRequest {
                    noItems(nil, error)
                }
        })
    }
    
  
    func getRewardsData() {
        
        AppServiceManager.doRequest(path: .rewards, method: .get, success: { [weak self] (response) in
            
            guard let response = response as? [JSON], let membershipData = [AZMembershipModel].from(jsonArray: response) else {
                
                let err = NSError(domain: "somedomain", code: 123, userInfo: [NSLocalizedDescriptionKey : "generic_error_msg".localized()])
                self?.didGetMembershipRequest(nil, err)
                return
            }
            self?.didGetMembershipRequest(membershipData, nil)
            }, failure: { (error) in
                if let noItems = self.didGetMembershipRequest {
                    noItems(nil, error)
                }
        })
    }
    
    func checkOrder() {
        
        let userId = String(describing: ((Helper.getObjectDefault(key: Constants.userDefault.userData) as! UserDataModel).iD as! Int))
        
        AppServiceManager.doRequest(path: .hasMembership, method: .post, params: ["user_machine_id": userId], success: {
            [weak self] (responseObj) in
            
            guard let data = responseObj as? [String: String], let error = data["Error"] else { return }
            
            if error == "false"  {
                
                let err = NSError(domain: "somedomain", code: 123, userInfo: [NSLocalizedDescriptionKey : "generic_error_msg".localized()])
                self?.didCheckOrder(nil, err)
                return
            } else {
                Helper.showFloatAlert(title:  data["Msg"] as! String, subTitle: "", type: Constants.AlertType.Alertinfo)
                self?.didCheckOrder(data["Msg"], nil)
            }
            }, failure: { (error) in
                self.didCheckOrder(nil, error)
        })
    }
    
    
    
    
}


