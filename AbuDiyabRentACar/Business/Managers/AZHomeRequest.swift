//
//  AZHomeRequest.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 12/4/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import Gloss


class AZHomeRequest {
    
    var didGetHomeSliderImages: (([AZHomeSliderModel]?, Error?) -> Void)!
    
    func getHomeSliderImages() {
        
        AppServiceManager.doRequest(path: .homeSlider, method: .get, success: { [weak self] (response) in
            
            guard let response = response as? [JSON], let homeSlider = [AZHomeSliderModel].from(jsonArray: response) else {
                
                let err = NSError(domain: "somedomain", code: 123, userInfo: [NSLocalizedDescriptionKey : "generic_error_msg".localized()])
                self?.didGetHomeSliderImages(nil, err)
                return
            }
            self?.didGetHomeSliderImages(homeSlider, nil)
            }, failure: { (error) in
                if let noItems = self.didGetHomeSliderImages {
                    noItems(nil, error)
                }
        })
    }
    
    
    var didGetHomeOffers: (([AZHomeOffersModel]?, Error?) -> Void)!
    
    func getHomeOffers() {
        
        AppServiceManager.doRequest(path: .homeOffers, method: .get, success: { [weak self] (response) in
            
            guard let response = response as? [JSON], let homeOffer = [AZHomeOffersModel].from(jsonArray: response) else {
                
                let err = NSError(domain: "somedomain", code: 123, userInfo: [NSLocalizedDescriptionKey : "generic_error_msg".localized()])
                self?.didGetHomeOffers(nil, err)
                return
            }
            self?.didGetHomeOffers(homeOffer, nil)
            }, failure: { (error) in
                if let noItems = self.didGetHomeOffers {
                    noItems(nil, error)
                }
        })
    }
    
}
