//
//  AZBranchesRequest.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 11/30/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import Gloss


class AZBranchesRequest {
    
    var didGetBranchesList: (([AZBranchesModel]?, Error?) -> Void)!
    
    func getBranchesList() {
        
        AppServiceManager.doRequest(path: .branches, method: .get, success: { [weak self] (response) in
            
            guard let response = response as? [JSON], let branches = [AZBranchesModel].from(jsonArray: response) else {

                let err = NSError(domain: "somedomain", code: 123, userInfo: [NSLocalizedDescriptionKey : "generic_error_msg".localized()])
                self?.didGetBranchesList(nil, err)
                return
            }
            self?.didGetBranchesList(branches, nil)
        }, failure: { (error) in
            if let noItems = self.didGetBranchesList {
                noItems(nil, error)
            }
        })
    }

}
