//
//  AZUrls.swift
//  AbuDiyabRentACar
//
//  Created by Manar Magdy on 11/30/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import Alamofire

enum AZUrls {
    
    enum UrlEnvironment  {
        case baseUrl
        func getBaseUrl() -> String {
            switch self {
            case .baseUrl:
                return "https://abudiyab.com.sa/"
            }
        }
    }
    
    enum Path {
        
        case login
        case register
        case nationality
        case forgetPassword
        case resetPassword
        case viewProfile
        case editProfile
        case fleet(catId: String)
        case filter
        case membership
        case hasMembership
        case applyMembership
        case rewards
        case contactUs
        case branches
        case homeSlider
        case homeOffers
        case orderHistory
        case orderCredit
        case orderCash
        case checkOrder
        
        
        var absolutePath: String {
            switch self {
            case .login:
                return "wp-json/custom/login"
            case .register:
                return "api.php?res=reg"
            case .nationality:
                return "api.php?res=ntly"
            case .forgetPassword:
                return "api.php?res=reset"
            case .resetPassword:
                return "api.php?res=resetpassword"
            case .viewProfile:
                return "api.php?res=user&profile=enabled"
            case .editProfile:
                return "api.php?res=user&update=enabled"
            case .fleet(let catId):
                return "api.php?cat_id=\(catId)"
            case .filter:
                return "api.php?res=cat"
            case .branches:
                return "api.php?res=location"
            case .homeSlider:
                return "api.php?res=home&slider=enabled"
            case .homeOffers:
                return "api.php?res=home&offers=enabled"
            case .contactUs:
                return "api.php?res=contact"
            case .membership:
                return "api.php?res=membership&content=enabled"
            case .hasMembership:
                return "api.php?res=membership&apply=check"
            case .applyMembership:
                return "api.php?res=membership&apply=enabled"
            case .orderHistory:
                return "api.php?res=history"
            case .orderCredit:
                return "api.php?res=bankpayment"
            case .rewards:
                return "api.php?res=membership&point=enabled"
            case .orderCash:
                return "api.php?res=order"
            case .checkOrder:
                return "api.php?res=order&&ordered=already"
                
                
                
            }
        }
    }
    
    
    
    enum HttpMethod: String {
        case get = "GET"
        case post = "POST"
        case put = "PUT"
    }
}

