//
//	AZBranch.swift
//
//	Create by Manar Magdy on 30/11/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

//	The "Swift - Struct - Gloss" support has been made available by CodeEagle
//	More about him/her can be found at his/her website: https://github.com/CodeEagle

import Foundation 
import Gloss

//MARK: - AZBranch
public struct AZBranch: Glossy {

	public let phone1 : String!
	public let phone2 : String!
	public let timing : String!
	public let latitude : String!
	public let longitude : String!
	public let name : String!



	//MARK: Decodable
	public init?(json: JSON){
		phone1 = "Phone1" <~~ json
		phone2 = "Phone2" <~~ json
		timing = "Timing" <~~ json
		latitude = "latitude" <~~ json
		longitude = "longitude" <~~ json
		name = "name" <~~ json
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
		"Phone1" ~~> phone1,
		"Phone2" ~~> phone2,
		"Timing" ~~> timing,
		"latitude" ~~> latitude,
		"longitude" ~~> longitude,
		"name" ~~> name,
		])
	}

}