//
//	AZTicker.swift
//
//	Create by Manar Magdy on 8/12/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

//	The "Swift - Struct - Gloss" support has been made available by CodeEagle
//	More about him/her can be found at his/her website: https://github.com/CodeEagle

import Foundation 
import Gloss

//MARK: - AZTicker
public struct AZTicker: Glossy {

	public let points : String!



	//MARK: Decodable
	public init?(json: JSON){
		points = "points" <~~ json
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
		"points" ~~> points,
		])
	}

}