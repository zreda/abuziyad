//
//	AZHomeOffersModel.swift
//
//	Create by Manar Magdy on 30/11/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

//	The "Swift - Struct - Gloss" support has been made available by CodeEagle
//	More about him/her can be found at his/her website: https://github.com/CodeEagle

import Foundation 
import Gloss

//MARK: - AZHomeOffersModel
public struct AZHomeOffersModel: Glossy {

	public let descriptionField : String!
	public let id : Int!
	public let image : String!
	public let regularPrice : String!
	public let salePrice : String!
	public let title : String!



	//MARK: Decodable
	public init?(json: JSON){
		descriptionField = "description" <~~ json
		id = "id" <~~ json
		image = "image" <~~ json
		regularPrice = "regular_price" <~~ json
		salePrice = "sale_price" <~~ json
		title = "title" <~~ json
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
		"description" ~~> descriptionField,
		"id" ~~> id,
		"image" ~~> image,
		"regular_price" ~~> regularPrice,
		"sale_price" ~~> salePrice,
		"title" ~~> title,
		])
	}

}