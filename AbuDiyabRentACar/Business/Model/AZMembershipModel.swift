//
//	AZMembershipModel.swift
//
//	Create by Manar Magdy on 8/12/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

//	The "Swift - Struct - Gloss" support has been made available by CodeEagle
//	More about him/her can be found at his/her website: https://github.com/CodeEagle

import Foundation 
import Gloss

//MARK: - AZMembershipModel
public struct AZMembershipModel: Glossy {

	public let clrCode : String!
	public let image : String!
	public let tickers : [AZTicker]!
	public let title : String!



	//MARK: Decodable
	public init?(json: JSON){
		clrCode = "clr_code" <~~ json
		image = "image" <~~ json
		tickers = "tickers" <~~ json
		title = "title" <~~ json
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
		"clr_code" ~~> clrCode,
		"image" ~~> image,
		"tickers" ~~> tickers,
		"title" ~~> title,
		])
	}

}