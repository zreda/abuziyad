//
//	AZHomeSliderModel.swift
//
//	Create by Manar Magdy on 30/11/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

//	The "Swift - Struct - Gloss" support has been made available by CodeEagle
//	More about him/her can be found at his/her website: https://github.com/CodeEagle

import Foundation 
import Gloss

//MARK: - AZHomeSliderModel
public struct AZHomeSliderModel: Glossy {

	public let sliderImage : String!



	//MARK: Decodable
	public init?(json: JSON){
		sliderImage = "slider_image" <~~ json
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
		"slider_image" ~~> sliderImage,
		])
	}

}