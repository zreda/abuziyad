//
//	AZBranchesModel.swift
//
//	Create by Manar Magdy on 30/11/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

//	The "Swift - Struct - Gloss" support has been made available by CodeEagle
//	More about him/her can be found at his/her website: https://github.com/CodeEagle

import Foundation 
import Gloss

//MARK: - AZBranchesModel
public struct AZBranchesModel: Glossy {

	public let city : String!
	public let branch : [AZBranch]!
	public let latitude : String!
	public let longitude : String!



	//MARK: Decodable
	public init?(json: JSON){
		city = "City" <~~ json
		branch = "branch" <~~ json
		latitude = "latitude" <~~ json
		longitude = "longitude" <~~ json
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
		"City" ~~> city,
		"branch" ~~> branch,
		"latitude" ~~> latitude,
		"longitude" ~~> longitude,
		])
	}

}