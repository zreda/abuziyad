//
//  CalenderPopUp.swift
//  Orcas
//
//  Created by Zeinab Reda on 7/6/17.
//  Copyright © 2017 Zeinab Reda. All rights reserved.
//

import UIKit
import AAPopUp
import FSCalendar

protocol CalenderPopUpDelegate{
    func finishPickDate(date:String)
}


class CalenderPopUp: UIViewController ,  FSCalendarDataSource, FSCalendarDelegate{
    
    @IBOutlet weak var calender: FSCalendar!
    @IBOutlet weak var bkView: UIView!
    static var popup:AAPopUp?
    var dateStr:String = ""
    static var delegate:CalenderPopUpDelegate?
    static var index:IndexPath?
    static var sectionIndex:Int?
    var attributeId:Int?
    
    override func viewDidLoad() {
        bkView.layer.cornerRadius = 10.0
        bkView.layer.masksToBounds = true
        calender.delegate = self
        calender.dataSource = self
//        self.hideKeyboard()

    }
    
    
    
    @IBAction func addBtnTapped(_ sender: Any) {
        
        CalenderPopUp.delegate?.finishPickDate(date: dateStr)
        CalenderPopUp.popup?.dismissPopUpView()
    }
    
    @IBAction func cancelBtnTapped(_ sender: Any) {
        
        CalenderPopUp.popup?.dismissPopUpView()
        
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        
        let result = formatter.string(from: date)
        
        dateStr = result
        
    }
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
       
            let formatter = DateFormatter()
            formatter.dateFormat = "dd MMM yyyy"
            
            let result = formatter.string(from: date)
            
            dateStr = result
     
        
    }
    
}
