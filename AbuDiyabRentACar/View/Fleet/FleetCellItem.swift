//
//  FleetCellItem.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 11/30/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import SDWebImage

class FleetCellItem: UICollectionViewCell {
    @IBOutlet weak var fleet_img: UIImageView!
    @IBOutlet weak var fleet_title: UILabel!
    @IBOutlet weak var fleet_price: UILabel!
    @IBOutlet weak var fleet_details: UILabel!
    
    @IBOutlet weak var detailBtn: UIButton!
    
    @IBOutlet weak var bookBtn: UIButton!
    
    func configureCell(item:FleetModelRespopnse)
    {
     
        fleet_img.sd_setImage(with: URL(string: (item.thumbnail!)), placeholderImage: #imageLiteral(resourceName: "default_img"))
        fleet_title.text = item.title
        
        fleet_price.text = (item.regularPrice ?? "") + " SAR".localized()
        fleet_details.text = item.content

    }
    
   
    

}
