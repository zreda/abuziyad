//
//  OrderTableViewCell.swift
//  AbuDiyabRentACar
//
//  Created by Zeinab Reda on 12/10/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {

    @IBOutlet weak var orderNo: UILabel!
    
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var showBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(order:OrderModelResponse)
    {
        orderNo.text = String(describing:order.orderId as! Int)
        total.text = order.total
        status.text = order.status
        date.text = order.date
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
